FROM amd64/ubuntu:22.04

WORKDIR /usr/src/app

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    libpcre3-dev \
    tcl8.6 \
    tcllib \
    tclx8.4 \
    sqlite3 \
    openssl \
    supervisor \
    cron && \
    mkdir -p /var/log/supervisor && \
    rm -rf /var/cache/apt

COPY ./docker_conf/supervisord/zelax_controller.conf /etc/supervisor/conf.d/supervisord.conf
COPY ./docker_conf/cron.d/zelax-sdwan-gui /etc/cron.d/zelax-sdwan-gui
COPY zelax-sdwan-controller.deb ./
COPY zelax-sdwan-gui.deb ./
COPY cert ./

RUN dpkg -i zelax-sdwan-controller.deb zelax-sdwan-gui.deb && \
    sdwancfg certificates set ca rootCA.crt own controller.crt key controller.key && \
    rm -rf /usr/src/app
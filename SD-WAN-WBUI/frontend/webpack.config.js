const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
require('babel-polyfill');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  favicon: './public/favicon.ico',
  template: './public/index.html',
  filename: path.resolve(__dirname, './dist/index.html'),
  inject: 'body',
  templateParameters(compilation, assets, options) {
    return {
      compilation,
      webpack: compilation.getStats().toJson(),
      webpackConfig: compilation.options,
      htmlWebpackPlugin: {
        files: assets,
        options,
      },
      process,
    };
  },
});

module.exports = {
  entry: ['babel-polyfill', './src/app.js'],
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'dist/assets'),
    publicPath: '/assets/',
    filename: 'app.[fullhash].js',
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000,
    ignored: ['node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: true,
            },
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.css$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: true,
            },
          },
          'css-loader',
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.(png|jpg|gif|svg|ttf|woff2|woff|eot)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[contenthash].[ext]',
          esModule: false,
        },
      },
    ],
  },
  plugins: [
    new webpack.ProgressPlugin(),
    new CleanWebpackPlugin(),
    HtmlWebpackPluginConfig,
    new VueLoaderPlugin(),
    new ESLintPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
    new MiniCssExtractPlugin({
      filename: 'app.[hash].css',
    }),
    new CopyPlugin({
      patterns: [
        { from: './public/static', to: './' },
      ],
    }),
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      '@store': path.resolve(__dirname, './src/store'),
      '@components': path.resolve(__dirname, './src/components'),
    },
    extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js', '.vue'],
  },
  performance: {
    hints: false,
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: false,
    port: 8080,
    historyApiFallback: true,
  },
};

if (process.env.NODE_ENV === 'development') {
  module.exports.devtool = 'source-map';
}

if (process.env.NODE_ENV === 'production') {
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: 'production',
      },
    }),
  ]);
  module.exports.output.filename = 'app.min.js';
}

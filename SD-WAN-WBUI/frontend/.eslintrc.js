module.exports = {
  env: {
    browser: true,
    es2017: true,
  },
  extends: [
    'plugin:vue/essential',
    'airbnb-base',
  ],
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 8,
    sourceType: 'module',
    allowImportExportEverywhere: true,
  },
  plugins: [
    'vue',
  ],
  rules: {
    'no-restricted-syntax': 'off',
    'no-continue': 'off',
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          [
            '@',
            './src',
          ],
          [
            '@store',
            './src/store',
          ],
          [
            '@components',
            './src/components',
          ],
        ],
      },
    },
  },
};

/**
 * Обрабатываем перетаскивание модалки.
 * dragEvent - событие перетаскивания (onmousemove)
 * downEvent - событие начала перетаскивания (mousedown)
 * originalRect - начальные координаты и размеры модалки
 */
function Drag(dragEvent, downEvent, modal, originalRect) {
  // Минимальное расстояние, на которое модалку можно поднести к краям окна
  const fieldsX = -originalRect.width * 0.75;
  const fieldsY = -originalRect.height * 0.75;

  // Высчитываем новые координаты модалки
  let newLeft = originalRect.left + dragEvent.clientX - downEvent.clientX;
  let newTop = originalRect.top + dragEvent.clientY - downEvent.clientY;

  // Не позволяем унести модалку за края окна
  if (newLeft < fieldsX) {
    newLeft = fieldsX;
  } else if (newLeft + originalRect.width > window.innerWidth - fieldsX) {
    newLeft = window.innerWidth - fieldsX - originalRect.width;
  }
  if (newTop < fieldsY) {
    newTop = fieldsY;
  } else if (newTop + originalRect.height > window.innerHeight - fieldsY) {
    newTop = window.innerHeight - fieldsY - originalRect.height;
  }

  // Устанавливаем новые координаты
  /* eslint-disable no-param-reassign */
  modal.style.marginLeft = `${newLeft}px`;
  modal.style.marginTop = `${newTop}px`;
  /* eslint-enable no-param-reassign */
}

/**
 * Инициализируем перетаскиваемое модальное окно.
 * event - событие mousedown в модальном окне
 */
function DraggableModal(event) {
  const modalHeader = event.target.closest('.draggable-modal .modal-header');
  if (modalHeader) {
    const modal = event.target.closest('.draggable-modal');
    const rect = modal.getBoundingClientRect();

    // Устанавливаем обработчик начала перетаскивания
    document.onmousemove = function DragStart(dragEvent) {
      Drag(dragEvent, event, modal, rect);
    };

    // Устанавливаем обработчик конца перетаскивания
    document.onmouseup = function DragEnd() {
      document.onmousemove = null;
      document.onmouseup = null;
    };
  }
}

export default DraggableModal;

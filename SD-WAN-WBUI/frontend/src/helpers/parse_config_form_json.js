import ModelNotification from '@/models/notification';
import ModelCpeConfigParam from '@/models/cpe_config_param';
import store from '@/store';

// Массив, в который добавляются form_id
let uniqueFormIds;

// Массив, в который добавляются field_id
let uniqueFieldIds;

// Строка, с которой начинается сообщение об ошибке парсинга
const errorPrefix = 'Pasring config_form.json failed, errors list:\n';
// Строка, содержащая список проблем парсинга
let errorsListStr;

// Допустимые имена свойств объекта формы
const formObjProps = [
  'form_id',
  'form_header',
  'key_field_ids',
  'key_options_format',
  'add_new',
  'form_width',
  'form_columns',
];

// Допустимые имена свойств объекта колонки формы
const columnObjProps = [
  'column_width',
  'column_fields',
];

// Допустимые имена свойств объекта поля
const fieldObjProps = [
  'field_id',
  'param_name',
  'label',
  'field_conditions',
  'input_type',
  'config_type',
  'dropdown_options',
  'default_value',
  'param_is_deleted_by_default',
  'param_value',
  'required',
  'read_only_field',
  'is_list',
  'outer_width',
  'inner_width',
];

// Варианты типов полей формы
const InputTypes = Object.freeze({
  DROPDOWN: 'dropdown',
  CHECKBOX: 'checkbox',
  TEXTAREA: 'textarea',
  STRING: 'string',
  INTEGER: 'integer',
  IPV4_ADDRESS: 'ipv4-address',
  ETHERNET_ADDRESS: 'ethernet-address',
  HIDDEN: 'hidden',
});

/**
 * Добавляем сообщение errorStr в errorsListStr
 */
function AddErrorToList(errorStr) {
  errorsListStr += `• ${errorStr}\n`;
}

/**
 * Возвращаем поле с fieldID или undefined, если такого поля нет
 */
function FindFieldByID(formsArr, fieldID) {
  for (const formObj of formsArr) {
    for (const columnObj of formObj.form_columns) {
      const findResult = columnObj.column_fields.find((id) => id === fieldID);
      return findResult;
    }
  }
}

/**
 * Валидируем объект поля fieldObj
 */
function validateFieldObject(fieldObj) {
  // Проверяем, что нет неизвестных полей
  for (const property in fieldObj) {
    if (fieldObjProps.find((prop) => prop === property) === undefined) {
      AddErrorToList(`unknown field property "${property}"`);
    }
  }

  // Проверяем наличие и тип field_id
  if (typeof fieldObj.field_id !== 'string') {
    AddErrorToList(`field "${fieldObj}": property "field_id" is mandatory and must be a string`);
  }

  // Проверяем уникальность field_id
  if (uniqueFieldIds.find((id) => id === fieldObj.field_id) !== undefined) {
    AddErrorToList(`field_id "${fieldObj.field_id}" is not unique`);
  }

  // Добавляем field_id этого поля в uniqueFieldIds
  uniqueFieldIds.push(fieldObj.field_id);

  // Проверяем наличие и тип param_name
  if (typeof fieldObj.param_name !== 'string') {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "param_name" is mandatory and must be a string');
  }

  // Проверяем наличие и тип label
  if (typeof fieldObj.label !== 'string') {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "label" is mandatory and must be a string');
  }

  // Проверяем field_conditions
  if (fieldObj.field_conditions !== undefined) {
    // Если field_conditions есть, то это должен быть непустой массив
    if (!Array.isArray(fieldObj.field_conditions)
        || fieldObj.field_conditions.length === 0) {
      AddErrorToList(`field "${fieldObj.field_id}": `
        + 'property "field_conditions" must be a non-empty array');
    }

    // Перебираем объекты в field_conditions
    fieldObj.field_conditions.forEach((condObj) => {
      // Проверяем наличие и тип condition_field_id
      if (typeof condObj.condition_field_id !== 'string') {
        AddErrorToList(`field "${fieldObj.field_id}": "field_conditions" array: `
          + 'property "condition_field_id" is mandatory and must be a string');
      }

      // Проверяем наличие condition_field_value
      if (condObj.condition_field_value === undefined) {
        AddErrorToList(`field "${fieldObj.field_id}": "field_conditions" array: `
          + 'property "condition_field_value" is mandatory');
      }
    });
  }

  // Проверяем наличие и тип input_type
  if (typeof fieldObj.input_type !== 'string') {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "input_type" is mandatory and must be a string');
  }

  // Проверяем значение input_type
  if (Object.values(InputTypes).find((type) => type === fieldObj.input_type) === undefined) {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + `property "input_type" unknown value "${fieldObj.input_type}"`);
  }

  // Проверяем наличие и тип config_type
  if (typeof fieldObj.config_type !== 'string') {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "config_type" is mandatory and must be a string');
  }

  // Проверяем значение config_type
  const configTypes = Object.values(ModelCpeConfigParam.GetParamTypeEnum());
  if (configTypes.find((type) => type === fieldObj.config_type) === undefined) {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + `property "config_type" unknown value "${fieldObj.config_type}"`);
  }

  // Проверяем dropdown_options
  if (fieldObj.input_type === InputTypes.DROPDOWN) {
    if (fieldObj.dropdown_options === undefined
        || !Array.isArray(fieldObj.dropdown_options)
        || fieldObj.dropdown_options.length === 0) {
      AddErrorToList(`field "${fieldObj.field_id}": `
        + 'when "input_type" is set to value "dropdown", '
        + 'property "dropdown_options" is mandatory and must be a non-empty array');
    } else {
      // Перебираем объекты в dropdown_options
      fieldObj.dropdown_options.forEach((optObj) => {
        // Проверяем наличие value
        if (optObj.value === undefined) {
          AddErrorToList(`field "${fieldObj.field_id}": "dropdown_options" array: `
            + 'property "value" is mandatory');
        }

        // Проверяем наличие и тип text
        if (typeof optObj.text !== 'string') {
          AddErrorToList(`field "${fieldObj.field_id}": "dropdown_options" array: `
            + 'property "text" is mandatory and must be a string');
        }
      });
    }
  } else if (fieldObj.dropdown_options !== undefined) {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "dropdown_options" cannot present '
      + 'when "input_type" is not set to value "dropdown"');
  }

  // Проверяем наличие default_value
  if (fieldObj.default_value === undefined) {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "default_value" is mandatory');
  }

  // Проверяем наличие и тип param_is_deleted_by_default
  if (fieldObj.param_is_deleted_by_default !== undefined
      && typeof fieldObj.param_is_deleted_by_default !== 'boolean') {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "param_is_deleted_by_default" must be a boolean');
  }

  // Проверяем param_value
  if (fieldObj.param_value !== undefined) {
    // Если param_value есть, то это должен быть непустой массив
    if (!Array.isArray(fieldObj.param_value)
        || fieldObj.param_value.length === 0) {
      AddErrorToList(`field "${fieldObj.field_id}": `
        + 'property "param_value" must be a non-empty array');
    }

    // Перебираем объекты в param_value
    fieldObj.param_value.forEach((condObj) => {
      // Проверяем наличие input_value
      if (condObj.input_value === undefined) {
        AddErrorToList(`field "${fieldObj.field_id}": "param_value" array: `
          + 'property "input_value" is mandatory');
      }

      // Проверяем наличие и значение config_action
      const actionTypes = Object.values(ModelCpeConfigParam.GetConfigActionEnum());
      if (actionTypes.find((action) => action === condObj.config_action) === undefined) {
        AddErrorToList(`field "${fieldObj.field_id}": "param_value" array: `
          + `property "config_action" unknown value "${condObj.config_action}"`);
      }

      // Проверяем наличие и тип config_value_field_id
      const setVal = ModelCpeConfigParam.GetConfigActionEnum().SET;
      if (condObj.config_action === setVal) {
        if (condObj.config_value_field_id === undefined
            || typeof condObj.config_value_field_id !== 'string') {
          AddErrorToList(`field "${fieldObj.field_id}": "param_value" array: `
            + `when "config_action" is set to value "${setVal}", `
            + 'property "config_value_field_id" is mandatory and must be a string');
        }
      } else if (condObj.config_value_field_id !== undefined) {
        AddErrorToList(`field "${fieldObj.field_id}": "param_value" array: `
          + 'property "config_value_field_id" cannot present '
          + `when "input_type" is not set to value "${setVal}"`);
      }
    });
  }

  // Проверяем наличие и тип required
  if (typeof fieldObj.required !== 'boolean') {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "required" is mandatory and must be a boolean');
  }

  // Проверяем тип read_only_field
  if (fieldObj.read_only_field !== undefined
      && typeof fieldObj.read_only_field !== 'boolean') {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "read_only_field" must be a boolean');
  }

  // Проверяем тип is_list
  if (fieldObj.is_list !== undefined
      && typeof fieldObj.is_list !== 'boolean') {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "is_list" must be a boolean');
  }

  // Проверяем наличие, тип и допустимые значения outer_width
  if (typeof fieldObj.outer_width !== 'number'
      || fieldObj.outer_width < 1
      || fieldObj.outer_width > 12) {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "outer_width" is mandatory and must be a number between 1 and 12');
  }

  // Проверяем наличие, тип и допустимые значения inner_width
  if (typeof fieldObj.inner_width !== 'number'
      && fieldObj.inner_width !== null
      && (fieldObj.inner_width < 1
          || fieldObj.inner_width > 12)) {
    AddErrorToList(`field "${fieldObj.field_id}": `
      + 'property "inner_width" is mandatory and must be either a null '
      + 'or a number between 1 and 12');
  }
}

/**
 * Валидируем объект колонки columnObj
 */
function validateColumnObject(formId, columnObj, columnIndex) {
  // Проверяем, что нет неизвестных полей
  for (const property in columnObj) {
    if (columnObjProps.find((prop) => prop === property) === undefined) {
      AddErrorToList(`unknown columns property "${property}"`);
    }
  }

  // Проверяем наличие, тип и допустимые значения column_width
  if (typeof columnObj.column_width !== 'number'
      || columnObj.column_width < 1
      || columnObj.column_width > 12) {
    AddErrorToList(`form "${formId}" object: ${columnIndex}'th column object `
      + 'property "column_width" is mandatory and must be a number between 1 and 12');
  }

  // Если массив column_fields есть, то он не должен быть пустым
  if (columnObj.column_fields !== undefined
      && Array.isArray(columnObj.column_fields)
      && columnObj.column_fields.length === 0) {
    AddErrorToList(`form "${formId}" object: `
      + 'property "column_fields" cannot be an empty array');
  }

  // Перебираем поля в колонке
  columnObj.column_fields.forEach((fieldObj) => {
    validateFieldObject(fieldObj);
  });
}

/**
 * Валидируем объект формы formObj
 */
function validateFormObject(formObj, formIndex) {
  // Проверяем, что нет неизвестных полей
  for (const property in formObj) {
    if (formObjProps.find((prop) => prop === property) === undefined) {
      AddErrorToList(`unknown property "${property}" in form "${formObj.form_id}" object`);
    }
  }

  // Проверяем наличие и тип form_id
  if (typeof formObj.form_id !== 'string') {
    AddErrorToList(`${formIndex}'th form object: `
      + 'property "form_id" is mandatory and must be a string');
  }

  // Проверяем уникальность form_id
  if (uniqueFormIds.find((id) => id === formObj.form_id) !== undefined) {
    AddErrorToList(`form_id "${formObj.form_id}" is not unique`);
  }

  // Добавляем form_id этого поля в uniqueFormIds
  uniqueFormIds.push(formObj.form_id);

  // Проверяем наличие и тип form_header
  if (typeof formObj.form_header !== 'string') {
    AddErrorToList(`form "${formObj.form_id}" object: `
      + 'property "form_header" is mandatory and must be a string');
  }

  // Проверяем наличие и тип key_field_ids
  if (!Array.isArray(formObj.key_field_ids)) {
    AddErrorToList(`form "${formObj.form_id}" object: `
      + 'property "key_field_ids" is mandatory and must be an array');
  }

  // Проверяем наличие и тип key_options_format
  if (typeof formObj.key_options_format !== 'string') {
    AddErrorToList(`form "${formObj.form_id}" object: `
      + 'property "key_options_format" is mandatory and must be a string');
  }

  // Проверяем наличие и тип add_new
  if (typeof formObj.add_new !== 'boolean') {
    AddErrorToList(`form "${formObj.form_id}" object: `
      + 'property "add_new" is mandatory and must be a boolean');
  }

  // Проверяем наличие, тип и допустимые значения form_width
  if (typeof formObj.form_width !== 'number'
      || formObj.form_width < 1
      || formObj.form_width > 12) {
    AddErrorToList(`form "${formObj.form_id}" object: `
      + 'property "form_width" is mandatory and must be a number between 1 and 12');
  }

  // Проверяем наличие и тип form_columns, form_columns не должен быть пустым
  if (!Array.isArray(formObj.form_columns)
      && formObj.form_columns.length === 0) {
    AddErrorToList(`form "${formObj.form_id}" object: `
      + 'property "form_columns" is mandatory and must be a non-empty array');
  }

  // Перебираем объекты колонок в форме
  formObj.form_columns.forEach((columnObj, columnIndex) => {
    validateColumnObject(formObj.form_id, columnObj, columnIndex);
  });
}

/**
 * Получаем и валидируем содержимое файла с описанием формата вывода форм
 * InputTypes - допустимые значения input_type
 */
async function ParseConfigFormJson() {
  uniqueFieldIds = [];
  uniqueFormIds = [];
  errorsListStr = '';

  const response = await fetch('/assets/config_form.json');

  let data;
  try {
    // Пытаемся распарсить как JSON
    data = await response.json();
  } catch (e) {
    AddErrorToList(`not a valid json: ${e}`);
  }

  // На верхнем уровне файла должен быть массив форм
  if (!Array.isArray(data)) {
    AddErrorToList('forms array not found');
  }

  // Массив форм не должен быть пустым
  if (data.length === 0) {
    AddErrorToList('forms array is empty');
  }

  // Перебираем объекты форм
  data.forEach((formObj, formIndex) => {
    validateFormObject(formObj, formIndex);
  });

  /* Проверяем существование field_id, используемых в свойствах форм key_field_ids,
   * key_options_format и в свойствах полей param_name,
   * field_conditions.condition_field_id и param_value.config_value_field_id */
  data.forEach((formObj) => {
    /* Проверяем все field_id из key_field_ids, соответствующие им поля
     * должны быть в той же форме и это не должны быть поля-списки */
    formObj.key_field_ids.forEach((keyFieldId) => {
      let foundField;
      for (const columnObj of formObj.form_columns) {
        foundField = columnObj.column_fields.find((fieldObj) => fieldObj.field_id === keyFieldId);
        if (foundField !== undefined) {
          break;
        }
      }
      if (foundField === undefined) {
        AddErrorToList(`form "${formObj.form_id}": `
          + `field_id "${keyFieldId}" is referenced in "key_field_ids" `
          + 'but is not present in any field of this form');
      } else if (foundField.is_list) {
        AddErrorToList(`form "${formObj.form_id}": `
          + `field "${keyFieldId}" with "is_list" `
          + 'flag in value "true" could not be referenced in "key_field_ids"');
      }
    });

    if (formObj.key_options_format.length > 0) {
      // Вычлиняем все field_id из key_options_format
      const regExp = new RegExp(/'([^']*?)'/g);
      const idMatches = regExp.exec(formObj.key_options_format);
      if (idMatches !== null) {
        // Перебираем все field_id, найденные в key_options_format
        for (let match = 1; match < idMatches.length; match += 1) {
          const matchedId = idMatches[match];
          // Ищем field_id из key_options_format среди key_field_ids
          if (formObj.key_field_ids.find((id) => id === matchedId) === undefined) {
            AddErrorToList(`form "${formObj.form_id}": `
              + `field_id "${matchedId}" is referenced in "key_options_format" `
              + 'but is not present in "key_field_ids" of this form');
          }
        }
      }
    }

    // Перебираем поля во всех колонках
    formObj.form_columns.forEach((columnObj) => {
      columnObj.column_fields.forEach((fieldObj) => {
        // Вычлиняем все field_id из param_name
        const regExp = new RegExp(/'([^']*?)'/g);
        const idMatches = regExp.exec(fieldObj.param_name);
        if (idMatches !== null) {
          // Перебираем все field_id, найденные в param_name
          for (let match = 1; match < idMatches.length; match += 1) {
            const matchedId = idMatches[match];
            // Ищем field_id из param_name среди uniqueFieldIds
            if (uniqueFieldIds.find((id) => id === matchedId) === undefined) {
              AddErrorToList(`field "${fieldObj.field_id}": `
                + `field_id "${matchedId}" is referenced in "param_name" `
                + 'but is not present in any field');
            }
          }
        }

        // Проверяем все field_id из field_conditions
        if (fieldObj.field_conditions !== undefined) {
          fieldObj.field_conditions.forEach((condObj) => {
            // Ищем field_id из field_conditions среди uniqueFieldIds
            if (uniqueFieldIds.find((id) => id === condObj.condition_field_id) === undefined) {
              AddErrorToList(`field "${fieldObj.field_id}": `
                + `field_id "${condObj.condition_field_id}" is referenced `
                + 'in "field_conditions" but is not present in any field');
            } else { // Проверяем, что это не поле-список
              const foundField = FindFieldByID(data, condObj.condition_field_id);
              if (foundField !== undefined
                  && foundField.is_list) {
                AddErrorToList(`form "${formObj.form_id}": `
                  + `field "${condObj.condition_field_id}" with "is_list" `
                  + 'flag in value "true" could not be referenced in "field_conditions"');
              }
            }
          });
        }

        // Проверяем все field_id из param_value
        if (fieldObj.param_value !== undefined) {
          fieldObj.param_value.forEach((paramValObj) => {
            // Ищем field_id из param_value среди uniqueFieldIds
            if (paramValObj.config_value_field_id !== undefined) {
              if (uniqueFieldIds.find(
                (id) => id === paramValObj.config_value_field_id,
              ) === undefined) {
                AddErrorToList(`field "${fieldObj.field_id}": `
                  + `field_id "${paramValObj.config_value_field_id}" is referenced `
                  + 'in "config_value_field_id" but is not present in any field');
              } else { // Проверяем, что это не поле-список
                const foundField = FindFieldByID(data, paramValObj.config_value_field_id);
                if (foundField !== undefined
                    && foundField.is_list) {
                  AddErrorToList(`form "${formObj.form_id}": `
                    + `field "${paramValObj.config_value_field_id}" with "is_list" `
                    + 'flag in value "true" could not be referenced in "config_value_field_id"');
                }
              }
            }
          });
        }
      });
    });
  });

  // Если с парсингом были проблемы, то формируем сообщение об ошибке
  if (errorsListStr.length > 0) {
    store().dispatch('notifications/add', new ModelNotification({
      message: `${errorPrefix}${errorsListStr}`,
      type: ModelNotification.GetNotificationTypeEnum().DANGER,
    }));
  }

  return data;
}

export { InputTypes };
export { ParseConfigFormJson };

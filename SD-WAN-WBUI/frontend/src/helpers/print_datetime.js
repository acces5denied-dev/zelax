/**
 * Переводим timestamp в дату формата YYYY-MM-DD hh:mm:ss
 * Если timestamp null, то возвращаем пустую строку.
 */
function PrintDateTime(timestamp) {
  if (timestamp === null) {
    return '';
  }

  const timeZoneOffset = (new Date()).getTimezoneOffset() * 60000; // смещение временной зоны в мс
  return (new Date(timestamp - timeZoneOffset)).toISOString().slice(0, 19).replace('T', ' ');
}

export default PrintDateTime;

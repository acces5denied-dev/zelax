import Vue from 'vue';
import ModelNotification from '@/models/notification';
import store from '@/store';
import router from '@/router';

/**
 * Показ нотификаций при получении ошибок исполнения js-кода
 */
function SetErrorNotifications() {
  /* window.onerror отловит ошибки в асинхронном коде в обработчиках Vue, например:
   * mounted() {
   *   setTimeout(() => {
   *     console.log(undefined_var.undefined_field);
   *   }, 3000);
   * }
   *
   * В message при этом попадает только ScriptError, поэтому выводим такое
   * сообщение "общего вида".
   */
  window.onerror = function windowError(/* message, source, lineno, colno, error */) {
    store().dispatch('notifications/add', new ModelNotification({
      message: 'Check browser console for more info.',
      type: ModelNotification.GetNotificationTypeEnum().DANGER,
      internal: true,
    }));
  };

  /* router.onError отловит ошибки вне обработчиков Vue, например:
   * <script>
   * import ...
   * console.log(undefined_var.undefined_field);
   * export ...
   * </script>
   */
  router.onError((error) => {
    store().dispatch('notifications/add', new ModelNotification({
      message: `Router error occurred: ${error}`,
      type: ModelNotification.GetNotificationTypeEnum().DANGER,
      internal: true,
    }));

    // eslint-disable-next-line no-console
    console.error(error);
  });

  // errorHandler отловит основные ошибки в обработчиках Vue
  Vue.config.errorHandler = function innerError(err, vm, info) {
    store().dispatch('notifications/add', new ModelNotification({
      message: `Error: ${err.toString()}\nInfo: ${info}`,
      type: ModelNotification.GetNotificationTypeEnum().DANGER,
      internal: true,
    }));

    // eslint-disable-next-line no-console
    console.error(err);
  };

  /* Об ошибках в асинхронных запросах сигнализирует ErrorNotification()
   * из /frontend/src/bridges/api/api_helpers.js */
}

export default SetErrorNotifications;

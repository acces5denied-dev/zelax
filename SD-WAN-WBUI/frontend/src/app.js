/** Инициализация */
import Vue from 'vue';
import Layout from './layout/main.vue';
import router from './router';

/** vuex */
import store from './store';

/** Импорт стилей */
import './scss/app.scss';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

/** Вывод уведомлений об ошибках */
import SetErrorNotifications from './helpers/error_notifications';

SetErrorNotifications();

/** vue */
Vue.config.productionTip = false;
new Vue({
  store,
  router,
  render: (h) => h(Layout),
  components: {
    Layout,
  },
}).$mount('#app');

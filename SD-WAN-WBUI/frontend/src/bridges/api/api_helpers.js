import store from '@/store';
import ModelNotification from '@/models/notification';

/**
 * Отправить нотификацию об отловленной ошибке выполнения запроса axios
 * error - отловленная ошибка
 * failedActionMsg - строка, описывающая неудавшийся запрос
 * Функция вернёт отправленную нотификацию.
 */
function ErrorNotification(error, failedActionMsg) {
  // Формируем нотификацию
  let notification = null;
  if (error.response) {
    // Был отправлен реквест, но сервер ответил статусом вне диапазона 2xx
    notification = new ModelNotification({
      message: `Unable to ${failedActionMsg} (status ${error.response.status})`,
      type: ModelNotification.GetNotificationTypeEnum().DANGER,
    });
    if (error.response.data) {
      notification.message += `: ${error.response.data}`;
    }
  } else if (error.request) {
    /* Был отправлен реквест, но не было получено ответа.
     * error.request - инстанс XMLHttpRequest */
    notification = new ModelNotification({
      message: `Unable to ${failedActionMsg}, no response. `
        + 'Check your internet connection and check that '
        + 'SD-WAN gui, SD-WAN proxy and SD-WAN controller services are running. '
        + 'If problem is not resolved, please send following information '
        + `to the support service: ${error}`,
      type: ModelNotification.GetNotificationTypeEnum().DANGER,
    });
  } else {
    /* Произошла ошибка при попытке отправить реквест.
     * Или есть ошибка в коде обработки ответа, например:
     * > await axios({
     * >   ...
     * > }).then((response) => {
     * >   console.log(undefined_var.undefined_field);
     * >   ...
     * > }).catch((error) => {
     * >   ...
     * > });
     */
    notification = new ModelNotification({
      message: `Unable to ${failedActionMsg}, `
        + `either failed to make the request or failed to handle the response: ${error}`,
      type: ModelNotification.GetNotificationTypeEnum().DANGER,
      internal: true,
    });
  }

  store().dispatch('notifications/add', notification);

  return notification;
}

export default ErrorNotification;

import axios from 'axios';
import store from '@/store';
import ModelNotification from '@/models/notification';
import ErrorNotification from './api_helpers';
import CpeListApi from './cpe_list_api';

/**
 * Класс для выполнения запросов конфигурации CPE к бэкэнду.
 */
class CpeConfigApi {
  constructor() {
    // Имя элемента в массиве links модели ModelCpe, значение которого содержит ссылку на конфиг
    this.apiLinkRelName = 'cpe_config';

    return this;
  }

  /**
   * Обрабатываем response, содержащий пришедшие обновления конфига CPE.
   * Возвращаем объект с полями:
   *   error - true, если возникла ошибка, иначе false
   *   update_num - актуальное значение update
   *   updates - массив полученных параметров
   */
  static handleCpeConfigUpdates(response, cpeId, updateNum) {
    // Ответ без валидного тела - внутренняя ошибка
    if (response.data === undefined
        || response.data.data === undefined) {
      store().dispatch('notifications/add', new ModelNotification({
        message: `Unable to get CPE "${cpeId}" config, no data: ${response.data}`,
        type: ModelNotification.GetNotificationTypeEnum().DANGER,
        internal: true,
      }));

      return {
        error: true,
        update_num: null,
        updates: [],
      };
    }

    const newUpdateNum = response.data.update;

    // Ответ с устаревшим update num - внутренняя ошибка
    if (updateNum !== null
        && updateNum > newUpdateNum) {
      store().dispatch('notifications/add', new ModelNotification({
        message: `Received update num "${newUpdateNum}" `
          + `is obsolete (current update num is "${updateNum}")`,
        type: ModelNotification.GetNotificationTypeEnum().DANGER,
        internal: true,
      }));

      return {
        error: true,
        update_num: updateNum,
        updates: [],
      };
    }

    // Ответ с текущим update num - игнорируем полученные данные
    if (updateNum !== null
        && updateNum === newUpdateNum) {
      store().dispatch('notifications/add', new ModelNotification({
        message: `CPE "${cpeId}" config loaded with current update num: ${newUpdateNum}.`
          + ' No updates.',
        type: ModelNotification.GetNotificationTypeEnum().INFO,
      }));

      return {
        error: false,
        update_num: updateNum,
        updates: [],
      };
    }

    let paramsCount = 0;
    if (response.data.data[0] !== undefined
        && response.data.data[0].config !== undefined) {
      paramsCount = response.data.data[0].config.length;
    }

    // Получен нормальный ответ - шлём нотификацию
    store().dispatch('notifications/add', new ModelNotification({
      message: `CPE "${cpeId}" config loaded, update num: ${newUpdateNum}, `
        + `${paramsCount} params updated`,
      type: ModelNotification.GetNotificationTypeEnum().INFO,
    }));

    // Если запрашивали полный конфиг, то старый конфиг очищаем
    if (updateNum === null) {
      store().commit('cpeConfig/clear');
    }

    // Сохраняем пришедшие данные
    let updates = [];
    if (paramsCount > 0) {
      updates = response.data.data[0].config;
      updates.forEach((apiCpeConfigParamObj) => {
        store().commit('cpeConfig/processUpdates', apiCpeConfigParamObj);
      });
    }

    return {
      error: false,
      update_num: newUpdateNum,
      updates,
    };
  }

  /**
   * Принимаем cpeId.
   * Возвращаем объект с полями:
   *   api_link - ссылка на получение и применение конфига для CPE cpeId.
   *              null, если не удалось получить ссылку.
   *   sync_state - статус синхронизации CPE cpeId.
   *              null, если не удалось получить статус.
   */
  async getCpeData(cpeId) {
    // Запрашиваем список CPE
    return CpeListApi().getCpeList().then(() => {
      // В store ищем нужное CPE
      const cpe = store().state.cpeList.cpe_list.find(
        (listItem) => listItem.cpe_id === cpeId,
      );

      // Не нашли CPE в store
      if (cpe === undefined) {
        store().dispatch('notifications/add', new ModelNotification({
          message: `Unable to get API link to CPE "${cpeId}" config, no such CPE`,
          type: ModelNotification.GetNotificationTypeEnum().DANGER,
          critical: true,
        }));

        return {
          api_link: null,
          sync_state: null,
        };
      }

      const syncState = cpe.synchronization_state;

      // Ищем ссылку на конфиг в массиве links этого CPE
      const apiLinkObj = cpe.links.find(
        (link) => link.rel === this.apiLinkRelName,
      );

      // Не нашли ссылку
      if (apiLinkObj === undefined) {
        store().dispatch('notifications/add', new ModelNotification({
          message: `Unable to get API link to CPE "${cpeId}" config, link not found in storage`,
          type: ModelNotification.GetNotificationTypeEnum().DANGER,
          internal: true,
        }));

        return {
          api_link: null,
          sync_state: syncState,
        };
      }

      const apiLink = apiLinkObj.uri;

      // Получен нормальный ответ - шлём нотификацию
      store().dispatch('notifications/add', new ModelNotification({
        message: `API link to CPE "${cpeId}" config received: ${apiLink}`,
        type: ModelNotification.GetNotificationTypeEnum().INFO,
      }));

      return {
        api_link: apiLink,
        sync_state: syncState,
      };
    });
  }

  /**
   * Загрузка конфигурации выбранного CPE.
   * Возвращаем объект в формате handleCpeConfigUpdates().
   */
  async getCpeConfig(cpeId, uri, updateNum) {
    const results = await axios({
      url: uri,
      responseType: 'text',
      params: {
        update: updateNum,
      },
    // eslint-disable-next-line arrow-body-style
    }).then((response) => {
      return this.constructor.handleCpeConfigUpdates(response, cpeId, updateNum);
    }).catch((error) => {
      ErrorNotification(error, `get CPE "${cpeId}" config`);

      return {
        error: true,
        update_num: null,
        updates: [],
      };
    });

    return results;
  }

  /**
   * Отправка изменений конфигурации выбранного CPE.
   * Возвращаем объект в формате handleCpeConfigUpdates().
   */
  async updateCpeConfig(uri, updateNum, cpeConfigDiff) {
    const results = await axios({
      method: 'PATCH',
      url: uri,
      responseType: 'text',
      params: {
        update: updateNum,
      },
      data: JSON.stringify(cpeConfigDiff),
    }).then((response) => {
      // Получен нормальный ответ - шлём нотификацию
      store().dispatch('notifications/add', new ModelNotification({
        message: `CPE "${cpeConfigDiff.cpe_id}" config updated`,
        type: ModelNotification.GetNotificationTypeEnum().SUCCESS,
      }));

      // Обновления конфига CPE уже должны быть в ответе
      return this.constructor.handleCpeConfigUpdates(response, cpeConfigDiff.cpe_id, updateNum);
    }).catch((error) => {
      ErrorNotification(error, `update CPE "${cpeConfigDiff.cpe_id}" config`);

      // Запрашиваем изменения конфига CPE чтобы увидеть актуальные данные
      return this.getCpeConfig(cpeConfigDiff.cpe_id, uri, updateNum).then(
        (getResponse) => {
          // Даже если изменения получили успешно, всё равно говорим, что была ошибка.
          // eslint-disable-next-line no-param-reassign
          getResponse.error = true;
          return getResponse;
        },
      );
    });

    return results;
  }

  /**
   * Замена конфигурации выбранного CPE на cpeConfig.
   * Возвращаем объект в формате handleCpeConfigUpdates().
   */
  async replaceCpeConfig(uri, cpeConfig) {
    const results = await axios({
      method: 'PUT',
      url: uri,
      responseType: 'text',
      data: JSON.stringify(cpeConfig),
    }).then((response) => {
      // Получен нормальный ответ - шлём нотификацию
      store().dispatch('notifications/add', new ModelNotification({
        message: `CPE "${cpeConfig.cpe_id}" config replaced`,
        type: ModelNotification.GetNotificationTypeEnum().SUCCESS,
      }));

      // Обновления конфига CPE уже должны быть в ответе
      return this.constructor.handleCpeConfigUpdates(response, cpeConfig.cpe_id, null);
    }).catch((error) => {
      ErrorNotification(error, `replace CPE "${cpeConfig.cpe_id}" config`);

      return {
        error: true,
        update_num: null,
        updates: [],
      };
    });

    return results;
  }
}

const cpeConfigApi = new CpeConfigApi();

export default () => (
  cpeConfigApi
);

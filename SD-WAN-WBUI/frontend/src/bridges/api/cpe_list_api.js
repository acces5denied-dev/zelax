import axios from 'axios';
import store from '@/store';
import ModelNotification from '@/models/notification';
import ErrorNotification from './api_helpers';

/**
 * Класс для выполнения запросов списка CPE к бэкэнду.
 */
class CpeListApi {
  constructor() {
    this.cpeListUpdateNum = null;

    this.cpeUri = '/api/cpe';

    return this;
  }

  /**
   * Обрабатываем response, содержащий пришедшие обновления списка CPE.
   */
  handleCpeListUpdates(response) {
    // Вернулся 205 статус - обнуляем свой update num и запрашиваем полный список
    if (response.status === 205) {
      store().dispatch('notifications/add', new ModelNotification({
        message: 'Unable to get CPE list, response status 205 - obsolete update num',
        type: ModelNotification.GetNotificationTypeEnum().WARNING,
      }));

      this.cpeListUpdateNum = null;
      this.getCpeList();
      return;
    }

    // Ответ без валидного тела - внутренняя ошибка
    if (response.data === undefined
          || response.data.data === undefined) {
      store().dispatch('notifications/add', new ModelNotification({
        message: `Unable to get CPE list, no data: ${response.data}`,
        type: ModelNotification.GetNotificationTypeEnum().DANGER,
        internal: true,
      }));
      return;
    }

    const newUpdateNum = response.data.update;

    // Ответ с устаревшим update num - внутренняя ошибка
    if (this.cpeListUpdateNum !== null
          && this.cpeListUpdateNum > newUpdateNum) {
      store().dispatch('notifications/add', new ModelNotification({
        message: `Received update num "${newUpdateNum}" is obsolete`
            + ` (current update num is "${this.cpeListUpdateNum}")`,
        type: ModelNotification.GetNotificationTypeEnum().DANGER,
        internal: true,
      }));
      return;
    }

    // Ответ с текущим update num - игнорируем полученные данные
    if (this.cpeListUpdateNum !== null
          && this.cpeListUpdateNum === newUpdateNum) {
      store().dispatch('notifications/add', new ModelNotification({
        message: `CPE list loaded with current update num: ${newUpdateNum}. No updates.`,
        type: ModelNotification.GetNotificationTypeEnum().SECONDARY,
      }));
      return;
    }

    // Получен нормальный ответ - шлём нотификацию
    store().dispatch('notifications/add', new ModelNotification({
      message: `CPE list loaded, new update num: ${newUpdateNum}, `
          + `${response.data.data.length} CPE updated`,
      type: ModelNotification.GetNotificationTypeEnum().INFO,
    }));

    // Если запрашивали полный список, то старый список очищаем
    if (this.cpeListUpdateNum === null) {
      store().commit('cpeList/clear');
    }

    // Обновляем cpeListUpdateNum
    if (newUpdateNum !== undefined) {
      this.cpeListUpdateNum = newUpdateNum;
    }

    // Сохраняем пришедшие данные
    response.data.data.forEach((apiCpeObj) => {
      store().commit('cpeList/processUpdates', apiCpeObj);
    });
  }

  /**
   * Загрузить список CPE
   */
  async getCpeList() {
    await axios({
      url: this.cpeUri,
      responseType: 'text',
      params: {
        update: this.cpeListUpdateNum,
      },
    }).then((response) => {
      this.handleCpeListUpdates(response);
    }).catch((error) => {
      ErrorNotification(error, 'get CPE list');
    });
  }

  /**
   * Отправить изменения не относящихся к конфигурации параметров CPE.
   * Нотификации об ошибках продублировать в errorNotifications
   */
  async updateCpe(uri, cpeDiff, errorNotifications) {
    await axios({
      method: 'PATCH',
      url: uri,
      responseType: 'text',
      params: {
        update: this.cpeListUpdateNum,
      },
      data: JSON.stringify(cpeDiff),
    }).then((response) => {
      // Получен нормальный ответ - шлём нотификацию
      store().dispatch('notifications/add', new ModelNotification({
        message: `CPE "${cpeDiff.cpe_id}" updated`,
        type: ModelNotification.GetNotificationTypeEnum().SUCCESS,
      }));

      // Обновления списка CPE уже должны быть в ответе
      this.handleCpeListUpdates(response);
    }).catch((error) => {
      const notification = ErrorNotification(error, `update CPE "${cpeDiff.cpe_id}"`);
      errorNotifications.push(notification);

      // Запрашиваем изменения списка CPE чтобы увидеть актуальные данные
      this.getCpeList();
    });
  }

  /**
   * Зарегистрировать новое CPE.
   * Нотификации об ошибках продублировать в errorNotifications
   */
  async RegisterNewCpe(cpeModel, errorNotifications) {
    await axios({
      method: 'PATCH',
      url: `${this.cpeUri}/${cpeModel.cpe_id}`,
      responseType: 'text',
      data: JSON.stringify(cpeModel),
      params: {
        update: this.cpeListUpdateNum,
      },
    }).then((response) => {
      // Получен нормальный ответ - шлём нотификацию
      store().dispatch('notifications/add', new ModelNotification({
        message: `new CPE "${cpeModel.cpe_id}" registered`,
        type: ModelNotification.GetNotificationTypeEnum().SUCCESS,
      }));

      // Обновления списка CPE уже должны быть в ответе
      this.handleCpeListUpdates(response);
    }).catch((error) => {
      const notification = ErrorNotification(error, `register new CPE "${cpeModel.cpe_id}`);
      errorNotifications.push(notification);

      // Запрашиваем изменения списка CPE чтобы увидеть актуальные данные
      this.getCpeList();
    });
  }

  /**
   * Забытие CPE
   */
  async ForgetCpe(cpeId, uri) {
    await axios({
      method: 'DELETE',
      url: uri,
      responseType: 'text',
      params: {
        update: this.cpeListUpdateNum,
      },
    }).then(() => {
      // Получен нормальный ответ - шлём нотификацию
      store().dispatch('notifications/add', new ModelNotification({
        message: `CPE "${cpeId}" forgotten`,
        type: ModelNotification.GetNotificationTypeEnum().SUCCESS,
      }));

      // Запрашиваем изменения списка CPE чтобы увидеть актуальные данные
      this.getCpeList();
    }).catch((error) => {
      ErrorNotification(error, `forget CPE "${cpeId}"`);

      // Запрашиваем изменения списка CPE чтобы увидеть актуальные данные
      this.getCpeList();
    });
  }
}

const cpeListApi = new CpeListApi();

export default () => (
  cpeListApi
);

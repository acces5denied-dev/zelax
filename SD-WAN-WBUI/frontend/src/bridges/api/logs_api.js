import axios from 'axios';
import store from '@/store';
import ModelNotification from '@/models/notification';
import ErrorNotification from './api_helpers';

/**
 * Класс для выполнения запросов лога к бэкэнду.
 */
class LogsApi {
  constructor() {
    this.cpeLogsUri = '/api/log';

    return this;
  }

  /**
   * Получить число строк в логе.
   */
  async getLogLinesCount(oldLinesCount) {
    const results = await axios({
      url: `${this.cpeLogsUri}`,
      responseType: 'text',
      params: {
        line_from: 1,
        line_to: 1,
      },
    }).then((response) => {
      // Ответ без валидного тела
      if (response.data === undefined
        || response.data.lines_total === undefined) {
        store().dispatch('notifications/add', new ModelNotification({
          message: `Unable to get log lines count, no data: ${response.data}`,
          type: ModelNotification.GetNotificationTypeEnum().DANGER,
          internal: true,
        }));
        return 0;
      }

      // Получен нормальный ответ - шлём нотификацию
      const newLinesCount = response.data.lines_total;
      if (oldLinesCount !== newLinesCount) {
        store().dispatch('notifications/add', new ModelNotification({
          message: `Log lines count updated: ${newLinesCount}`,
          type: ModelNotification.GetNotificationTypeEnum().INFO,
        }));
      } else {
        store().dispatch('notifications/add', new ModelNotification({
          message: `No new log lines, current lines count: ${newLinesCount}`,
          type: ModelNotification.GetNotificationTypeEnum().SECONDARY,
        }));
      }

      return newLinesCount;
    }).catch((error) => {
      ErrorNotification(error, 'get log lines count');
      return 0;
    });

    return results;
  }

  /**
   * Получить строки лога с lineFrom по lineTo.
   * Если pushFront - true, то записываем новые строки в начало лога, иначе в конец.
   * Если clear - true, то очищаем хранилище лога перед записью
   */
  async getLogs(lineFrom, lineTo, pushFront, clear) {
    const results = await axios({
      url: `${this.cpeLogsUri}`,
      responseType: 'text',
      params: {
        line_from: lineFrom,
        line_to: lineTo,
      },
    }).then((response) => {
      // Ответ без валидного тела
      if (response.data === undefined
          || response.data.data === undefined) {
        store().dispatch('notifications/add', new ModelNotification({
          message: `Unable to get log, no data: ${response.data}`,
          type: ModelNotification.GetNotificationTypeEnum().DANGER,
          internal: true,
        }));
        return 0;
      }

      // Получен нормальный ответ - шлём нотификацию
      store().dispatch('notifications/add', new ModelNotification({
        message: `Log updated, loaded lines from "${lineFrom}" to "${lineTo}"`,
        type: ModelNotification.GetNotificationTypeEnum().INFO,
      }));

      // Старый лог очищаем
      if (clear) {
        store().commit('logs/clear');
      }

      // Сохраняем пришедшие данные
      if (pushFront === true) {
        response.data.data.slice().reverse().forEach((apiLogLineObj) => {
          store().commit('logs/pushFront', apiLogLineObj);
        });
      } else {
        response.data.data.forEach((apiLogLineObj) => {
          store().commit('logs/pushBack', apiLogLineObj);
        });
      }

      return response.data.lines_total;
    }).catch((error) => {
      ErrorNotification(error, 'get log');
      return 0;
    });

    return results;
  }

  /**
   * Очистить лог
   */
  async clearLog() {
    const results = await axios({
      url: `${this.cpeLogsUri}`,
      method: 'PATCH',
      responseType: 'text',
      data: JSON.stringify({ lines_count: 0 }),
    }).then(() => {
      // Получен нормальный ответ - шлём нотификацию
      store().dispatch('notifications/add', new ModelNotification({
        message: 'Log cleared',
        type: ModelNotification.GetNotificationTypeEnum().SUCCESS,
      }));
    }).catch((error) => {
      ErrorNotification(error, 'clear log');
    });

    return results;
  }
}

const logsApi = new LogsApi();

export default () => (
  logsApi
);

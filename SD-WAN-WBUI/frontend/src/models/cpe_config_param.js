const ParamTypeEnum = Object.freeze({
  STRING: 'string',
  INTEGER: 'integer',
  BOOLEAN: 'boolean',
  IPV4_ADDRESS: 'ipv4-address',
  ETHERNET_ADDRESS: 'ethernet-address',
  REFERENCE: 'reference',
  COMPLEX: 'complex',
});

const UpdateStatusEnum = Object.freeze({
  CREATED: 'created',
  UPDATED: 'updated',
  DELETED: 'deleted',
});

const ConfigActionEnum = Object.freeze({
  SET: 'set',
  DELETE: 'delete',
});

// Статусы для подсветки параметров
const SyncStatusEnum = Object.freeze({
  UNCHANGED: '', // Параметр не менялся
  UNAPPLIED: 'warning', // Параметр был изменён, но не был применён
  APPLIED: 'success', // Параметр был применён
  UNEXPECTED: 'primary', /* Параметр был "неожиданно" применён (пользователь
    * не менял параметр, а параметр поменялся или пользователь менял параметр
    * в одно значение и тип, а применилось другое значение и тип) */
});

/**
 * Модель хранения одного параметра конфигурации одного CPE.
 */
class ModelCpeConfigParam {
  // Варианты значений config_type
  static GetParamTypeEnum() {
    return ParamTypeEnum;
  }

  // Варианты значений update_status, используются в формате приёма из API
  static GetUpdateStatusEnum() {
    return UpdateStatusEnum;
  }

  // Варианты значений config_action, используются в формате отправки через API
  static GetConfigActionEnum() {
    return ConfigActionEnum;
  }

  // Варианты значений sync_status (используются в GUI для подсветки параметров)
  static GetSyncStatusEnum() {
    return SyncStatusEnum;
  }

  constructor() {
    return this;
  }

  // ---------------------- Валидаторы ----------------------

  ValidateParamType() {
    if (this.config_type === null
        || this.config_type === undefined) {
      return true;
    }

    const result = Object.values(ModelCpeConfigParam.GetParamTypeEnum()).filter(
      (i) => i === this.config_type,
    ).length;
    if (result === 0) {
      // eslint-disable-next-line no-console
      console.error(`Unknown config param type "${this.config_type}"`
          + ` of config param "${this.config_name}"`);
      return false;
    }
    return true;
  }

  ValidateUpdateStatus() {
    if (this.update_status === null
        || this.update_status === undefined) {
      return true;
    }

    const result = Object.values(ModelCpeConfigParam.GetUpdateStatusEnum()).filter(
      (i) => i === this.update_status,
    ).length;
    if (result === 0) {
      // eslint-disable-next-line no-console
      console.error(`Unknown config param update status "${this.update_status}"`
          + ` of config param "${this.config_name}"`);
      return false;
    }
    return true;
  }

  Validate() {
    return this.ValidateParamType()
           && this.ValidateUpdateStatus();
  }

  // ---------------------- ----------------------

  /**
   * Обновляем данные текущей модели CPE пришедшими из API данными
   */
  UpdateFromApi(apiCpeConfigParamObj) {
    if (apiCpeConfigParamObj === null) {
      return;
    }

    this.config_name = apiCpeConfigParamObj.config_name;
    this.config_type = apiCpeConfigParamObj.config_type;
    if (this.config_type === ModelCpeConfigParam.GetParamTypeEnum().COMPLEX) {
      this.config_value = JSON.stringify(apiCpeConfigParamObj.config_value, null, 2);
    } else {
      this.config_value = apiCpeConfigParamObj.config_value;
    }
    this.read_only = apiCpeConfigParamObj.read_only;
    this.rejected = apiCpeConfigParamObj.rejected;
    this.mandatory = apiCpeConfigParamObj.mandatory;

    if (apiCpeConfigParamObj.update_status === undefined) {
      this.update_status = null;
    } else {
      this.update_status = apiCpeConfigParamObj.update_status;
    }

    this.Validate();
  }

  /**
   * Заполняем данные текущей модели данными из sourceModel
   */
  CloneModelCpeConfigParam(sourceModel) {
    if (sourceModel === null) {
      return;
    }

    this.config_name = sourceModel.config_name;
    this.config_type = sourceModel.config_type;
    this.config_value = sourceModel.config_value;
    this.read_only = sourceModel.read_only;
    this.rejected = sourceModel.rejected;
    this.mandatory = sourceModel.mandatory;

    this.update_status = sourceModel.update_status;
  }

  /**
   * Вернуть объект с данными текущей модели, но с именами и структурой полей,
   * соответствующими формату API для передачи изменений.
   * Если передали параметр configAction, то добавляем в возвращаемую модель
   * поле config_action в значении configAction.
   */
  GetApiCpeConfigParamObject(configAction = null) {
    const apiCpeConfigParamObject = {
      config_name: this.config_name,
      config_type: this.config_type,
    };
    if (configAction !== null) {
      apiCpeConfigParamObject.config_action = configAction;
    }

    if (this.config_type === ModelCpeConfigParam.GetParamTypeEnum().COMPLEX) {
      apiCpeConfigParamObject.config_value = JSON.parse(this.config_value);
    } else {
      apiCpeConfigParamObject.config_value = this.config_value;
    }

    return apiCpeConfigParamObject;
  }

  /**
   * Инициализируем поля, необходимые для отображения таблицы конфигурации
   */
  InitGuiFields() {
    // Массив префиксов свёрток, которым принадлежит данный параметр
    this.collapsed_by_prefix = [];

    /* config_name, разделённый по слэшам */
    this.splitted_config_name = [];

    /* Для каждого разделителя храним строку с соответствующим ему префиксом */
    this.separator_prefix = [];

    // true для удалённых параметров или целиком удалённых свёрток
    this.is_deleted = false;

    // Выставляем статус подкраски параметра
    this.sync_status = ModelCpeConfigParam.GetSyncStatusEnum().UNCHANGED;

    /* true, если данный объект - свёртка,
       false, если параметр конфигурации */
    this.is_expander = false;

    // Заполняем splitted_config_name и separator_prefix
    this.splitString(this.config_name);
  }

  /**
   * Разбиваем inputString на части по слешам.
   * Сами части записываем в splitted_config_name.
   * В separator_prefix "для каждого слеша" записываем ту часть
   * inputString, которая идёт до этого слеша.
   */
  splitString(inputString) {
    // Регуляркой отлавливаем слеши, не заключённые между квадратными скобками
    // eslint-disable-next-line no-useless-escape
    this.splitted_config_name = inputString.split(/\/(?![^\[]*\])/);

    this.separator_prefix = [];
    let str = '';
    this.splitted_config_name.forEach((namePart) => {
      str += `${namePart}/`;
      this.separator_prefix.push(str);
    });
  }

  /**
   * Преобразуем объект, служащий для хранения данных о параметре конфига,
   * в объект, служащий для хранения данных строки-свёртки в таблице конфигурации.
   *
   * targetSlashPrefix - какой именно префикс имён параметров попадает в эту свёртку.
   *
   * Поля, отражающие состав параметров в свёртке, ставим в значения по умолчанию.
   * Актуальные значения для таких полей должны быть выставлены после вызова
   * данной функции.
   */
  TurnToExpander(targetSlashPrefix) {
    /* config_name ради наличия уникального id. Пробел в нём, по идее,
       должен защищать от пересечения с именами реальных параметров. */
    this.config_name = `${targetSlashPrefix} expander`;
    this.config_type = '';
    this.config_value = '';

    this.collapsed_by_prefix = [];

    // Для свёрток тут храним expander_prefix, разделённый по слэшам.
    this.splitted_config_name = [];
    this.separator_prefix = [];

    this.is_expander = true;
    this.expander_prefix = targetSlashPrefix;

    /* Временно ставим эти поля в значения по умолчанию */
    this.sync_status = ModelCpeConfigParam.GetSyncStatusEnum().UNCHANGED;
    this.is_nested = false; // true для свёрток, в которых попала хотя бы одна другая свёртка
    this.is_deleted = true; // true для целиком удалённых свёрток.
    this.is_read_only = false; // true для свёрток, в которой только read-only параметры
    this.collapsed_params_count = 0; // Полный счётчик попавших в свёртку параметров (не свёрток)
    this.deleted_params_count = 0; /* Cчётчик попавших в свёртку параметров (не свёрток),
                                      которые можно удалить */
    this.not_deleted_params_count = 0; /* Счётчик попавших в свёртку не удалённых
                                          параметров (не свёрток) */

    // Заполняем splitted_config_name и separator_prefix
    this.splitString(this.expander_prefix);
  }
}

export default ModelCpeConfigParam;

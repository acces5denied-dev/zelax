const SyslogLevelValuesEnum = Object.freeze({
  SYSLOG_LEVEL_EMERGENCY: 0,
  SYSLOG_LEVEL_ALERT: 1,
  SYSLOG_LEVEL_CRITICAL: 2,
  SYSLOG_LEVEL_ERROR: 3,
  SYSLOG_LEVEL_WARNING: 4,
  SYSLOG_LEVEL_NOTICE: 5,
  SYSLOG_LEVEL_INFORMATIONAL: 6,
  SYSLOG_LEVEL_DEBUG: 7,
});

const SyslogLevelNamesEnum = Object.freeze([
  'emerg', // 0
  'alert', // 1
  'crit', // 2
  'err', // 3
  'warning', // 4
  'notice', // 5
  'info', // 6
  'debug', // 7
]);

/**
 * Модель хранения одной строки лога.
 */
class ModelLogLine {
  // Варианты цифровых представлений level
  static GetLogLevelValuesEnum() {
    return SyslogLevelValuesEnum;
  }

  // Варианты строковых представлений level
  static GetLogLevelNamesEnum() {
    return SyslogLevelNamesEnum;
  }

  constructor() {
    return this;
  }

  // ---------------------- Валидаторы ----------------------

  ValidateLevel() {
    if (this.level === null
        || this.level === undefined) {
      return true;
    }

    if (this.level < ModelLogLine.GetLogLevelValuesEnum().SYSLOG_LEVEL_EMERGENCY
        || this.level > ModelLogLine.GetLogLevelValuesEnum().SYSLOG_LEVEL_DEBUG) {
      // eslint-disable-next-line no-console
      console.error(`Unknown log line level "${this.level}"`
          + ` of log line with number "${this.num}"`);
      return false;
    }
    return true;
  }

  Validate() {
    return this.ValidateLevel();
  }

  // ---------------------- ----------------------

  /**
   * Обновляем данные текущей модели CPE пришедшими из api данными
   */
  UpdateFromApi(apiLogLineObj) {
    if (apiLogLineObj === null) {
      return;
    }

    this.timestamp = apiLogLineObj.timestamp;
    this.level = apiLogLineObj.level;
    this.num = apiLogLineObj.num;
    this.text = apiLogLineObj.text;

    this.Validate();
  }

  /**
   * Вернуть название уровня
   */
  GetLevelName() {
    return ModelLogLine.GetLogLevelNamesEnum()[this.level];
  }
}

export default ModelLogLine;

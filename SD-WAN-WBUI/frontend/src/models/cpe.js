const ConfigSrcEnum = Object.freeze({
  CPE: 'cpe',
  CONTROLLER: 'controller',
});

const SynchronizationStateEnum = Object.freeze({
  FORBIDDEN: 'Forbidden',
  PENDING: 'Pending',
  COMPLETE: 'Complete',
  FAILED: 'Failed',
  IN_PROGRESS: 'In Progress',
  SUSPENDED: 'Suspended',
});

/**
 * Модель хранения не относящихся к конфигурации данных одного устройства.
 */
class ModelCpe {
  // Максимальная длина cpe_id
  static GetCpeIdMaxLength() {
    return 255;
  }

  // Максимальная длина name
  static GetNameMaxLength() {
    return 63;
  }

  /**
   * Возвращается регулярное выражение, которое проверяет,
   * что в строке есть только разрешённые для имени устройства символы
   */
  static GetNameAllowedCharsRegExp() {
    return new RegExp(/^[а-яА-Яa-zA-Z0-9_\-%^№:+=@]*$/g);
  }

  // Варианты значений config_src
  static GetConfigSrcEnum() {
    return ConfigSrcEnum;
  }

  // Варианты значений synchronization_state
  static GetSynchronizationStateEnum() {
    return SynchronizationStateEnum;
  }

  constructor() {
    return this;
  }

  // ---------------------- Валидаторы ----------------------

  ValidateCpeId() {
    if (this.cpe_id.length > ModelCpe.GetCpeIdMaxLength()) {
      // eslint-disable-next-line no-console
      console.error(`Exceeded device ID max length (max length is "${this.GetCpeIdMaxLength()}",`
          + ` current length is "${this.cpe_id.length}") of the device with id "${this.cpe_id}"`);
      return false;
    }
    return true;
  }

  ValidateName() {
    if (this.name === undefined
        || this.name === null) {
      // Имени может не быть у подключившегося незарегистрированного устройства
      return true;
    }

    if (this.name.length > ModelCpe.GetNameMaxLength()) {
      // eslint-disable-next-line no-console
      console.error(`Exceeded name max length (max length is "${this.GetNameMaxLength()}",`
          + ` current length is "${this.name.length}") of the device "${this.name}"`);
      return false;
    }

    if (!this.name.match(ModelCpe.GetNameAllowedCharsRegExp())) {
      return false;
    }

    return true;
  }

  ValidateConfigSrc() {
    if (this.config_src === undefined
        || this.config_src === null) {
      return true;
    }

    const result = Object.values(ModelCpe.GetConfigSrcEnum()).filter(
      (i) => i === this.config_src,
    ).length;
    if (result === 0) {
      // eslint-disable-next-line no-console
      console.error(`Unknown config source "${this.config_src}"`
                    + ` of the device with id "${this.cpe_id}"`);
      return false;
    }
    return true;
  }

  ValidateSynchronizationState() {
    if (this.synchronization_state === undefined
        || this.synchronization_state === null) {
      return true;
    }

    const result = Object.values(ModelCpe.GetSynchronizationStateEnum()).filter(
      (i) => i === this.synchronization_state,
    ).length;
    if (result === 0) {
      // eslint-disable-next-line no-console
      console.error(`Unknown synchronization state "${this.synchronization_state}"`
                    + ` of the device with id "${this.cpe_id}"`);
      return false;
    }
    return true;
  }

  Validate() {
    return this.ValidateCpeId()
           && this.ValidateName()
           && this.ValidateConfigSrc()
           && this.ValidateSynchronizationState();
  }

  // ---------------------- ----------------------

  /**
   * Обновляем данные текущей модели устройства пришедшими из api данными
   */
  UpdateFromApi(apiCpeObj) {
    if (apiCpeObj === null) {
      return;
    }

    if (apiCpeObj.cpe_id !== undefined) {
      this.cpe_id = apiCpeObj.cpe_id;
    }

    if (apiCpeObj.connection !== undefined) {
      if (apiCpeObj.connection.ip !== undefined) {
        this.ip = apiCpeObj.connection.ip;
      }

      if (apiCpeObj.connection.port !== undefined) {
        this.port = apiCpeObj.connection.port;
      }

      if (apiCpeObj.connection.active !== undefined) {
        this.connection_active = apiCpeObj.connection.active;
      }
    }

    if (apiCpeObj.profile !== undefined) {
      /* Пришёл profile null - это только что забытое устройство
       * Пришёл profile {} и предыдущих данных в модели нет - это устройство
       * находится в забытом состоянии, а мы только что запросили полный конфиг */
      const isForgotten = apiCpeObj.profile === null
        || (apiCpeObj.profile.constructor === Object
            && Object.keys(apiCpeObj.profile).length === 0
            && this.registered === undefined);

      if (isForgotten) {
        this.forgotten = true;
        this.name = undefined;
        this.registered = undefined;
        this.config_src = undefined;
        this.synchronization_locked = undefined;
        this.synchronization_auto_lock = undefined;
        this.synchronization_state = undefined;
      } else {
        this.forgotten = false;

        if (apiCpeObj.profile.name !== undefined) {
          this.name = apiCpeObj.profile.name;
        }

        if (apiCpeObj.profile.registered !== undefined) {
          this.registered = apiCpeObj.profile.registered;
        }

        if (apiCpeObj.profile.config_src !== undefined) {
          this.config_src = apiCpeObj.profile.config_src;
        }

        if (apiCpeObj.profile.synchronization_state !== undefined) {
          this.synchronization_state = apiCpeObj.profile.synchronization_state;
        }

        if (apiCpeObj.profile.synchronization_flags !== undefined) {
          if (apiCpeObj.profile.synchronization_flags.locked !== undefined) {
            this.synchronization_locked = apiCpeObj.profile.synchronization_flags.locked;
          }

          if (apiCpeObj.profile.synchronization_flags.auto_lock !== undefined) {
            this.synchronization_auto_lock = apiCpeObj.profile.synchronization_flags.auto_lock;
          }
        }
      }
    }

    /* Массив links содержит ссылки на получение профиля устройства
     * и получение конфига устройства.
     * Подробнее - см. SD-WAN-WBUI/server/app.js */
    this.links = apiCpeObj.links;

    this.Validate();
  }

  /**
   * Заполняем данные текущей модели устройства данными из sourceModelCpe
   */
  CloneModelCpe(sourceModelCpe) {
    if (sourceModelCpe === null) {
      return;
    }

    this.cpe_id = sourceModelCpe.cpe_id;
    this.ip = sourceModelCpe.ip;
    this.port = sourceModelCpe.port;
    this.connection_active = sourceModelCpe.connection_active;
    this.name = sourceModelCpe.name;
    this.registered = sourceModelCpe.registered;
    this.forgotten = sourceModelCpe.forgotten;
    this.config_src = sourceModelCpe.config_src;
    this.synchronization_locked = sourceModelCpe.synchronization_locked;
    this.synchronization_auto_lock = sourceModelCpe.synchronization_auto_lock;
    this.synchronization_state = sourceModelCpe.synchronization_state;
    this.links = sourceModelCpe.links;
  }

  /**
   * Возвращаем новую модель, содержащую все поля, которые отличаются у текущей
   * модели и у otherModelCpe. Значения для полей возвращаемой модели берутся
   * из текущей модели. Возможные различия в полях links и forgotten игнорируются.
   */
  GetDiff(otherModelCpe) {
    const diff = new ModelCpe();

    if (this.cpe_id !== otherModelCpe.cpe_id) {
      diff.cpe_id = this.cpe_id;
    }

    if (this.ip !== otherModelCpe.ip) {
      diff.ip = this.ip;
    }

    if (this.port !== otherModelCpe.port) {
      diff.port = this.port;
    }

    if (this.connection_active !== otherModelCpe.connection_active) {
      diff.connection_active = this.connection_active;
    }

    if (this.name !== otherModelCpe.name) {
      diff.name = this.name;
    }

    if (this.registered !== otherModelCpe.registered) {
      diff.registered = this.registered;
    }

    if (this.config_src !== otherModelCpe.config_src) {
      diff.config_src = this.config_src;
    }

    if (this.synchronization_locked !== otherModelCpe.synchronization_locked) {
      diff.synchronization_locked = this.synchronization_locked;
    }

    if (this.synchronization_auto_lock !== otherModelCpe.synchronization_auto_lock) {
      diff.synchronization_auto_lock = this.synchronization_auto_lock;
    }

    if (this.synchronization_state !== otherModelCpe.synchronization_state) {
      diff.synchronization_state = this.synchronization_state;
    }

    return diff;
  }

  /**
   * Вернуть объект с данными текущей модели, но с именами и структурой полей,
   * соответствующими формату api.
   */
  GetApiCpeObject() {
    const apiCpeObj = {
      cpe_id: this.cpe_id,
    };

    if (this.ip !== undefined
        || this.port !== undefined
        || this.connection_active) {
      apiCpeObj.connection = {};

      if (this.ip !== undefined) {
        apiCpeObj.connection.ip = this.ip;
      }

      if (this.port !== undefined) {
        apiCpeObj.connection.port = this.port;
      }

      if (this.connection_active !== undefined) {
        apiCpeObj.connection.active = this.connection_active;
      }
    }

    if (this.name !== undefined
        || this.registered !== undefined
        || this.config_src !== undefined
        || this.synchronization_state !== undefined
        || this.synchronization_locked !== undefined
        || this.synchronization_auto_lock !== undefined) {
      apiCpeObj.profile = {};

      if (this.name !== undefined) {
        apiCpeObj.profile.name = this.name;
      }

      if (this.registered !== undefined) {
        apiCpeObj.profile.registered = this.registered;
      }

      if (this.config_src !== undefined) {
        apiCpeObj.profile.config_src = this.config_src;
      }

      if (this.synchronization_state !== undefined) {
        apiCpeObj.profile.synchronization_state = this.synchronization_state;
      }

      if (this.synchronization_locked !== undefined
          || this.synchronization_auto_lock !== undefined) {
        apiCpeObj.profile.synchronization_flags = {};

        if (this.synchronization_locked !== undefined) {
          apiCpeObj.profile.synchronization_flags.locked = this.synchronization_locked;
        }

        if (this.synchronization_auto_lock !== undefined) {
          apiCpeObj.profile.synchronization_flags.auto_lock = this.synchronization_auto_lock;
        }
      }
    }

    return apiCpeObj;
  }

  /**
   * Возвращаем true, если это устройство надо удалить из хранилища устройств
   */
  isDeleted() {
    return this.registered === undefined
           && (this.connection_active === undefined
               || this.connection_active === false);
  }
}

export default ModelCpe;

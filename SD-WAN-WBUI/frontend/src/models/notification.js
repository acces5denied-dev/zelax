import PrintDateTime from '@/helpers/print_datetime';

const NotificationTypeEnum = Object.freeze({
  PRIMARY: 'primary',
  SECONDARY: 'secondary', /* Заменяет собой предыдущую нотификацию, если у той тот же тип и текст,
                           * чтобы история нотификаций не захламлялась одинаковыми сообщениями. */
  SUCCESS: 'success', // Успешный ответ на событие, инициированное пользователем
  DANGER: 'danger', // Возникла ошибка. Приводит к выскакиванию попапа с ошибкой.
  WARNING: 'warning', // Предупреждение
  INFO: 'info', // Успешный ответ на событие, не инициированное пользователем
  LIGHT: 'light',
  DARK: 'dark',
});

/**
 * id, который будет присвоен следующему созданному объекту ModelNotification
 */
let nextId = 0;

/**
 * Модель хранит данные одной отображаемой пользователю нотификации.
 *
 * Поля модели:
 *   id - number, уникальный идентификатор нотификации
 *   type - string, тип нотификации, варианты значений в NotificationTypeEnum
 *   message - string, обязательное поле, текст нотификации
 *   critical - boolean, если значение true, то нотификация будет показана в незакрываемом попапе
 *   hidden - boolean, если true, то нотификация не показывается в панели уведомлений
 *   creation_time - date, время создания нотификации
 *   hide_timeout - number,хранит timeoutID таймера на скрытие нотификации из панели уведомлений
 *                  или null, если таймер не установлен
 */
class ModelNotification {
  // Варианты значений type
  static GetNotificationTypeEnum() {
    return NotificationTypeEnum;
  }

  /**
   * Создаём нотификацию на основе объекта params. Поля params:
   * type, message - соответствуют одноимённым полям модели
   * critical - соответствуют одноимённому полю модели.
   *            Можно не передавать, тогда флаг будет выставлен в значение false
   * internal - boolean, если true, то текст нотификации будет дополнен предисловием
   *            о внутренней ошибке.
   *            Можно не передавать, что будет проинтерпретировано также, как false.
   */
  constructor(params) {
    if (!this.ValidateInputParams(params)) {
      return null;
    }

    this.id = nextId;
    nextId += 1;

    this.type = params.type;

    if (params.internal) {
      this.message = 'Internal Error. Please send following information to the support service.\n'
        + `${params.message}`;
    } else {
      this.message = params.message;
    }

    this.critical = Boolean(params.critical);

    this.hidden = false;
    this.creation_time = new Date();
    this.hide_timeout = null;

    return this;
  }

  // ---------------------- Валидаторы ----------------------

  /**
   * Валидация входных параметров конструктора
   */
  // eslint-disable-next-line class-methods-use-this
  ValidateInputParams(params) {
    // Вилидация type
    if (params.type === undefined
        || params.type === null) {
      // eslint-disable-next-line no-console
      console.error('No notification type specified');
      return false;
    }

    const result = Object.values(ModelNotification.GetNotificationTypeEnum()).filter(
      (i) => i === params.type,
    ).length;
    if (result === 0) {
      // eslint-disable-next-line no-console
      console.error('Unknown notification type', params.type);
      return false;
    }

    // Вилидация message
    if (params.message === undefined
        || params.message === null
        || params.message === '') {
      // eslint-disable-next-line no-console
      console.error('No notification message specified');
      return false;
    }

    // Вилидация флага internal
    if (params.internal !== undefined
        && params.internal !== null
        && typeof params.internal !== 'boolean') {
      // eslint-disable-next-line no-console
      console.error('Unexpected type of notification "internal" flag', typeof params.internal);
      return false;
    }

    // Вилидация флага critical
    if (params.critical !== undefined
        && params.critical !== null
        && typeof params.critical !== 'boolean') {
      // eslint-disable-next-line no-console
      console.error('Unexpected type of notification "critical" flag', typeof params.critical);
      return false;
    }

    return true;
  }

  // ---------------------- ----------------------

  PrintCreationTime() {
    return PrintDateTime(this.creation_time);
  }
}

export default ModelNotification;

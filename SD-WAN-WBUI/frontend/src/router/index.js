import Vue from 'vue';
import VueRouter from 'vue-router';
import DefaultView from '@/views/default.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/gui/devices',
  },
  {
    path: '/index.html',
    redirect: '/gui/devices',
  },
  {
    path: '/gui/devices',
    component: DefaultView,
    children: [
      {
        path: '',
        component: () => import(/* webpackChunkName: 'devices' */ '@/views/devices.vue'),
      },
    ],
  },
  {
    path: '/gui/device_config/:id',
    component: DefaultView,
    children: [
      {
        path: '',
        component: () => import(/* webpackChunkName: 'device_config' */ '@/views/device_config.vue'),
      },
    ],
  },
  {
    path: '/gui/logs',
    props: (route) => ({
      line: route.query.line,
      level: route.query.level,
      tail: route.query.tail,
    }),
    component: DefaultView,
    children: [
      {
        path: '',
        component: () => import(/* webpackChunkName: 'logs' */ '@/views/logs.vue'),
      },
    ],
  },
  {
    path: '*',
    component: DefaultView,
    children: [
      {
        path: '',
        component: () => import(/* webpackChunkName: '404' */ '@/views/404.vue'),
      },
    ],
  },
];

/**
 * Vue Router связывает определённые компоненты со страницами, где те должны отображаться.
 */
const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;

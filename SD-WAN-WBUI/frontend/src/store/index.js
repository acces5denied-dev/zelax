import Vue from 'vue';
import Vuex from 'vuex';

import cpeList from './modules/cpe_list';
import cpeConfig from './modules/cpe_config';
import notifications from './modules/notifications';
import logs from './modules/logs';
import logSettings from './modules/log_settings';

Vue.use(Vuex);

/**
 * Vuex хранит отображаемые в компонентах данные и предоставляет интерфейс для
 * доступа к этим данным и их изменения.
 *
 * Слой actions на деле не используем в данном проекте - зовём mutations напрямую.
 */
const store = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    cpeList,
    cpeConfig,
    notifications,
    logs,
    logSettings,
  },
});

export default () => (
  store
);

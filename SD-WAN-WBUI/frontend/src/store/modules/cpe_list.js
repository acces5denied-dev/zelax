/* eslint-disable no-shadow */
import ModelCpe from '@/models/cpe';

// -------------------- state --------------------
const state = () => ({
  // Массив ModelCpe
  cpe_list: [],
});

// -------------------- getters --------------------
const getters = {
  list: (state) => state.cpe_list,
};

// -------------------- actions --------------------
const actions = {
};

// -------------------- mutations --------------------
const mutations = {
  /**
   * Очистить хранилище данных списка CPE
   */
  clear: (state) => {
    // eslint-disable-next-line no-param-reassign
    state.cpe_list = [];
  },

  /**
   * Обрабатываем пришедшие обновления списка CPE.
   * Удаляем, обновляем или создаём ModelCpe в state зависимости от содержимого apiCpeObj.
   * apiCpeObj - данные одного параметра в формате API.
   */
  processUpdates: (state, apiCpeObj) => {
    const newCpe = new ModelCpe();
    newCpe.UpdateFromApi(apiCpeObj);

    // Ищем в state предыдущую версию CPE
    const stateCpe = state.cpe_list.find((listItem) => listItem.cpe_id === newCpe.cpe_id);

    if (stateCpe) {
      // Обновляем данные CPE
      stateCpe.UpdateFromApi(apiCpeObj);

      // Удаляем CPE
      if (stateCpe.isDeleted()) {
        state.cpe_list.splice(state.cpe_list.indexOf(stateCpe), 1);
      }
    } else if (!newCpe.isDeleted()) {
      /* Первой полученной информацией о новом CPE может быть информация о том,
       * что CPE надо удалить из хранилища. Если это не так, то добавляем новое CPE */
      state.cpe_list.push(newCpe);
    }
  },
};

// -------------------- export --------------------
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

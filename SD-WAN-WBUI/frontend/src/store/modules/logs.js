/* eslint-disable no-shadow */
import ModelLogLine from '@/models/log_line';

// -------------------- state --------------------
const state = () => ({
  // Массив ModelLogLine
  logs: [],
});

// -------------------- getters --------------------
const getters = {
  list: (state) => state.logs,
};

// -------------------- actions --------------------
const actions = {
};

// -------------------- mutations --------------------
const mutations = {
  /**
   * Очистить хранилище лога
   */
  clear: (state) => {
    // eslint-disable-next-line no-param-reassign
    state.logs = [];
  },

  /**
   * Удалить из хранилища лога строку под номером lineNum
   */
  remove: (state, lineNum) => {
    // eslint-disable-next-line no-param-reassign
    state.logs = state.logs.filter(
      (line) => (line.num !== lineNum),
    );
  },

  /**
   * На основании apiLogLineObj создать ModelLogLine и добавить в начало хранилища лога
   */
  pushFront: (state, apiLogLineObj) => {
    const newLine = new ModelLogLine();
    newLine.UpdateFromApi(apiLogLineObj);
    state.logs.unshift(newLine);
  },

  /**
   * На основании apiLogLineObj создать ModelLogLine и добавить в конец хранилища лога
   */
  pushBack: (state, apiLogLineObj) => {
    const newLine = new ModelLogLine();
    newLine.UpdateFromApi(apiLogLineObj);
    state.logs.push(newLine);
  },
};

// -------------------- export --------------------
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

/* eslint-disable no-shadow */
import ModelCpeConfigParam from '@/models/cpe_config_param';

// -------------------- state --------------------
const state = () => ({
  // Массив ModelCpeConfigParam
  cpe_config_params: [],
});

// -------------------- getters --------------------
const getters = {
  list: (state) => state.cpe_config_params,
};

// -------------------- actions --------------------
const actions = {
};

// -------------------- mutations --------------------
const mutations = {
  /**
   * Очистить хранилище конфигурации CPE
   */
  clear: (state) => {
    // eslint-disable-next-line no-param-reassign
    state.cpe_config_params = [];
  },

  /**
   * Обрабатываем пришедшие обновления конфигурации.
   * Удаляем, обновляем или создаём ModelCpeConfigParam в state зависимости
   * от содержимого apiCpeConfigParamObj.
   * apiCpeConfigParamObj - данные одного параметра в формате приёма из API.
   */
  processUpdates: (state, apiCpeConfigParamObj) => {
    const newParam = new ModelCpeConfigParam();
    newParam.UpdateFromApi(apiCpeConfigParamObj);

    // Ищем в state предыдущую версию параметра
    const stateParam = state.cpe_config_params.find((param) => (
      param.config_name === newParam.config_name
      && param.cpe_id === newParam.cpe_id));

    if (newParam.update_status === ModelCpeConfigParam.GetUpdateStatusEnum().DELETED) {
      if (stateParam) { // delete param
        // eslint-disable-next-line no-param-reassign
        state.cpe_config_params = state.cpe_config_params.filter(
          (param) => (param.config_name !== stateParam.config_name),
        );
      } else {
        // eslint-disable-next-line no-console
        console.error(`Unable to delete config param "${newParam.config_name}" `
          + '- not found in storage');
      }
    } else if (newParam.update_status === ModelCpeConfigParam.GetUpdateStatusEnum().UPDATED) {
      if (stateParam) {
        // update param
        stateParam.UpdateFromApi(apiCpeConfigParamObj);
      } else {
        // eslint-disable-next-line no-console
        console.error(`Unable to update config param "${newParam.config_name}" - `
          + 'not found in storage');
      }
    } else if (newParam.update_status === ModelCpeConfigParam.GetUpdateStatusEnum().CREATED
              || newParam.update_status === null) {
      if (stateParam) {
        // eslint-disable-next-line no-console
        console.error(`Unable to add new config param "${newParam.config_name}" `
            + '- already found in storage');
      } else {
        // add new param
        state.cpe_config_params.push(newParam);
      }
    }
  },
};

// -------------------- export --------------------
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

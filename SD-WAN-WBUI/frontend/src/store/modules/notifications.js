/* eslint-disable no-shadow, no-param-reassign */
import ModelNotification from '@/models/notification';

// -------------------- state --------------------
const state = () => ({
  // Массив ModelNotification
  notifications_list: [],

  // Флаг true, если необходимо показать попап с историей нотификаций
  popup_required: false,
});

// -------------------- getters --------------------
const getters = {
  // Полный массив нотификаций
  list: (state) => state.notifications_list,

  // Массив нотификаций, отображающихся в панели уведомлений
  shownList: (state) => state.notifications_list.filter(
    (notification) => notification.hidden === false,
  ),

  // Последняя нотификация с типом DANGER
  lastError: (state) => {
    for (let i = state.notifications_list.length - 1; i >= 0; i -= 1) {
      if (state.notifications_list[i].type === ModelNotification.GetNotificationTypeEnum().DANGER) {
        return state.notifications_list[i];
      }
    }
    return undefined;
  },

  // Флаг true, если необходимо показать попап с историей нотификаций
  isPopupRequired: (state) => state.popup_required,
};

// -------------------- actions --------------------
const actions = {
  /**
   * notification - объект ModelNotification.
   *
   * Если notification - пустой объект, то ничего не делаем.
   * Иначе, показываем пользователю нотификацию с типом notification.type и текстом
   * notification.message, устанавливаем и запоминаем таймаут на её скрытие
   * из панели уведомлений.
   *
   * Если notification имеет type SECONDARY и совпадает по type и message
   * с последней нотификацией в хранилище, то notification в хранилище
   * не добавляем, вместо этого редактируем данные последней нотификации
   * в хранилище так, чтобы выглядело, как будто она только что получена.
   * (чтобы история нотификаций не захламлялась одинаковыми сообщениями по таймеру).
   *
   * Иначе, добавляем notification в хранилище. Если notification имеет type
   * DANGER, то выставляем флаг выскакивания попапа с историей нотификаций.
   */
  add: ({ commit, state, dispatch }, notification) => {
    // При создании нотификации возникла ошибка - не добавляем её в хранилище
    if (Object.keys(notification).length === 0) {
      return;
    }

    // Запоминаем последнюю нотификацию в хранилище
    let lastNotification = null;
    if (state.notifications_list.length > 0) {
      lastNotification = state.notifications_list[state.notifications_list.length - 1];
    }

    if (notification.type === ModelNotification.GetNotificationTypeEnum().SECONDARY
        && lastNotification !== null
        && notification.type === lastNotification.type
        && notification.message === lastNotification.message) {
      // Обновляем данные lastNotification
      commit('repeat', lastNotification);
      // Устанавливаем и запоминаем таймаут на скрытие lastNotification из панели уведомлений
      dispatch('setHideTimeout', lastNotification);
      return;
    }

    // Выставляем флаг popup_required
    if (notification.type === ModelNotification.GetNotificationTypeEnum().DANGER) {
      commit('setPopupRequirement');
    }

    // Добавляем notification в хранилище
    commit('push', notification);
    // Устанавливаем и запоминаем таймаут на скрытие notification из панели уведомлений
    dispatch('setHideTimeout', notification);
  },

  /**
   * Устанавливаем и запоминаем таймаут на скрытие
   * ModelNotification notification из панели уведомлений
   */
  setHideTimeout: ({ commit }, notification) => {
    const hideTimeoutID = setTimeout(() => {
      commit('hide', notification);
    }, 3000);

    commit('rememberHideTimeout', { notification, hideTimeoutID });
  },
};

// -------------------- mutations --------------------
const mutations = {
  /**
   * Очистить хранилище нотификаций
   */
  clear: (state) => {
    state.notifications_list.forEach((notification) => {
      clearTimeout(notification.hide_timeout);
    });
    state.notifications_list = [];
  },

  /**
   * Удалить из хранилища ModelNotification notification
   */
  remove: (state, notification) => {
    const index = state.notifications_list.indexOf(notification);
    if (index > -1) {
      clearTimeout(notification.hide_timeout);
      state.notifications_list.splice(index, 1);
    }
  },

  /**
   * Поменять данные ModelNotification notification так, чтобы выглядело,
   * как будто эта нотификация только что получена.
   *
   * После вызова этой мутации необходимо вызвать экшен setHideTimeout() для
   * установки нового таймера на скрытие notification из панели уведомлений.
   */
  repeat: (state, notification) => {
    clearTimeout(notification.hide_timeout);
    notification.creation_time = new Date();
    notification.hidden = false;
  },

  /**
   * Добавить в хранилище ModelNotification notification.
   * Удалить из хранилища самую старую нотификацию,
   * если количество нотификаций превышает установленный лимит хранения.
   */
  push: (state, notification) => {
    // Добавляем новую нотификацию в хранилище
    state.notifications_list.push(notification);

    // Храним только последние maxNotificationCount нотификаций
    const maxNotificationCount = 50;
    if (state.notifications_list.length > maxNotificationCount) {
      state.notifications_list.shift();
    }
  },

  /**
   * Запоминаем hideTimeoutID скрытия ModelNotification notification
   * из панели уведомлений
   */
  rememberHideTimeout: (state, { notification, hideTimeoutID }) => {
    notification.hide_timeout = hideTimeoutID;
  },

  /**
   * Скрыть нотификацию ModelNotification notification из панели уведомлений.
   */
  hide: (state, notification) => {
    notification.hidden = true;
  },

  /**
   * Установить флаг выскакивания попапа с историей нотификаций
   */
  setPopupRequirement: (state) => {
    state.popup_required = true;
  },

  /**
   * Сбросить флаг выскакивания попапа с историей нотификаций
   */
  clearPopupRequirement: (state) => {
    state.popup_required = false;
  },
};

// -------------------- export --------------------
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

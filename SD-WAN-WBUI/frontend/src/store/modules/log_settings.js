/* eslint-disable no-shadow */

/**
 * Модуль хранит настройки отображения лога.
 */

// -------------------- state --------------------
const state = () => ({
  /* Фильтр лога по уровню.
   * Начальное значение не является валидным значением фильтра по уровню,
   * в валидный вид его приводит компонент logs */
  log_level_filter: null,

  /* Номер верхней видимой строки лога.
   * Начальное значение не является валидным значением номера строки,
   * в валидный вид его приводит компонент logs */
  log_current_line: null,

  // Флаг режима "хвоста" (постоянной подзагрузки новых строк лога)
  log_tail_mode: null,
});

// -------------------- getters --------------------
const getters = {
  getLevelFilter: (state) => state.log_level_filter,
  getCurrentLine: (state) => state.log_current_line,
  getTailMode: (state) => state.log_tail_mode,
};

// -------------------- actions --------------------
const actions = {
};

// -------------------- mutations --------------------
const mutations = {
  setLevelFilter: (state, newLevel) => {
    // eslint-disable-next-line no-param-reassign
    state.log_level_filter = newLevel;
  },
  setCurrentLine: (state, newLine) => {
    // eslint-disable-next-line no-param-reassign
    state.log_current_line = newLine;
  },
  setTailMode: (state, tailMode) => {
    // eslint-disable-next-line no-param-reassign
    state.log_tail_mode = tailMode;
  },
};

// -------------------- export --------------------
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

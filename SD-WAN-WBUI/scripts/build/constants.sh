#!/bin/bash

# Основная директория для сборки
BUILD_DIR=$HOME/.zelax-sdwan-gui-build

# Временная директория, куда помещаются все файлы для последующей упаковки в DEB
PACKAGE_DIR=$BUILD_DIR/package

# Путь где будут хранится файлы GUI после установки
DATA_DIR="/usr/share/zelax-sdwan-gui"

# Версия nodeJS используемая для сборки пакета
NODE_VERSION="v14.18.1"

# Имя директории куда будет распакован nodeJS
NODE_PATH="node-${NODE_VERSION}-linux-x64"

# Имя файла архива nodeJS скачиваемого с sdnfilestore
NODE_FILE="${NODE_PATH}.tar.xz"

# Путь к бинарникам распакованного nodeJS
NODE_BIN_PATH="${BUILD_DIR}/${NODE_PATH}/bin"

# Команда для скачивания файлов с sdnfilestore
SMBGET="smbget -w zelax smb://build:build@sdnfilestore.zelax.local/"

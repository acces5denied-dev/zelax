#!/bin/bash

#Для нашего CA генерируем приватный ключ 2048-бит RSA и сертификат.
# Администратор CA должен хранить ключ в секрете от всех
openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 36500 -keyout root_ca.key -out root_ca.crt -subj "/C=RU/CN=SD_WAN_WBUI_CLIENT"

#Приватный ключ для приложения
openssl genrsa -out localhost.key 2048

#Запрос на подпись сертификата для приложения
openssl req -key localhost.key -new -out localhost.csr -subj "/C=RU/CN=localhost"

#Создаем файл с дополнительной информацией, которая будет добавлена в сертификат в процессе подписания
echo \
"authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
subjectAltName=@alt_names
[alt_names]
DNS.1=localhost
DNS.2=127.0.0.1
DNS.3=::1" \
> localhost.ext

#Подписываем сертификат приложения корневым сертификатом
openssl x509 -req -CA root_ca.crt -CAkey root_ca.key -in localhost.csr -out localhost.crt \
 -days 36500 -CAcreateserial -extfile localhost.ext
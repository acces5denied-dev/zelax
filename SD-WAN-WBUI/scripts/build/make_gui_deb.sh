#!/bin/bash

set -e

# Переменные хранящие пути к директориям
TARGET_DIR=`pwd` 							# Директория откуда запущен скрипт

# Путь к папке со скриптами где находится текущий скрипт
SCRIPTS_DIR=$(dirname `readlink -f "$0"`)

# Путь к папке репозитория
REPO_DIR=$(dirname $(dirname $SCRIPTS_DIR))

. ${SCRIPTS_DIR}/constants.sh

PACKAGE_DATA_DIR=${PACKAGE_DIR}${DATA_DIR}/

DEBIAN_DATA_DIR=${REPO_DIR}/scripts/deb-files

pkg() {
	out=`which ${1} | grep -c ${1}`
	if [ ${out} -eq 0 ]; then
	  echo "${1} command is not found, please install ${1} and re-run this script."
    exit
	fi
}

pkg git
pkg smbclient

# Удаляем, если есть старая версия временной директории сборки
rm -rf ${PACKAGE_DIR}

# Создаем директории для сборки
mkdir -p ${BUILD_DIR}
mkdir -p ${PACKAGE_DIR}
mkdir -p ${PACKAGE_DATA_DIR}

cd $BUILD_DIR
# Проверяем наличие nodeJS и устанавливаем в случае отсутствия
if [ ! -d $NODE_PATH ]; then
  if [ ! -f $NODE_FILE ]; then
    ${SMBGET}other_distributions/nodejs/${NODE_FILE}
  fi
  tar -xf ${NODE_FILE} --exclude=*.md --exclude=LICENSE -C .
  rm -f ${NODE_FILE}
fi

export PATH=${BUILD_DIR}/${NODE_PATH}/bin:$PATH

# Копируем файлы backend
cp -r ${REPO_DIR}/server/* ${PACKAGE_DATA_DIR}
rm ${PACKAGE_DATA_DIR}/package.json

#Создаем директорию для ssl сертификатов
#mkdir -p ${PACKAGE_DATA_DIR}/server/cert
#cd ${PACKAGE_DATA_DIR}/server/cert

# Собираем frontend
cd ${REPO_DIR}/frontend/
npm rebuild node-sass 1> /dev/null
npm cache verify 1> /dev/null
npm run build 1> /dev/null

# Копируем собранные файлы frontend. Делаем это уже после копирования файлов
# backend, так как backend может хранить в htdocs файлы-болванки на случай
# отсутствия собранных файлов от frontend.
mkdir -p ${PACKAGE_DATA_DIR}/htdocs
cp -r dist/* ${PACKAGE_DATA_DIR}/htdocs

# Копируем nodejs
cp -r ${BUILD_DIR}/node-${NODE_VERSION}-linux-x64 ${PACKAGE_DATA_DIR}/nodejs

# Копируем файлы DEBIAN
cp -r ${DEBIAN_DATA_DIR}/* ${PACKAGE_DIR}

CURRENT_DATE=`date +"%Y.%m.%d"`
COMMIT=$(git -C ${REPO_DIR} log -1 --pretty=%h)

# Получаем текущую версию сборки
if [ -f ${REPO_DIR}/version.txt ]; then
  VERSION=$(cat ${REPO_DIR}/version.txt)
else
  VERSION="${CURRENT_DATE}-${COMMIT}"
fi

# Записываем версию пакета в DEBIAN/control
echo "Version: ${VERSION}" >> ${PACKAGE_DIR}/DEBIAN/control

chmod -R 755 ${PACKAGE_DIR}/DEBIAN
dpkg-deb -Zgzip -b ${PACKAGE_DIR} ${TARGET_DIR}/zelax-sdwan-gui.${CURRENT_DATE}_${COMMIT}.deb

# Удаляем временную директорию сборки
rm -rf ${PACKAGE_DIR}

echo "Success"

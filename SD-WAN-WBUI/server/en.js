exports.JUST_MESSAGE = "{1}";
exports.HTTPD_INDEX_HTML_OPEN_ERROR = "Error while trying to open index.html file. {1}";
exports.HTTPD_OPEN_PORT_ERROR = "Something went wrong, while try to open port. {1}";
exports.HTTPD_OPEN_PORT_SUCCESS = "Server is listening on port {1}.";
exports.SDWANCFG_SOCKET_SEND_ERROR = "Error while send request to socket. {1}";
exports.SDWANCFG_SOCKET_ENDED = "Socket connection is ended.";
exports.SDWANCFG_SOCKET_OPEN = "Socket connection is open.";
exports.SDWANCFG_SOCKET_NOT_FOUND = "Socket file is not found.";

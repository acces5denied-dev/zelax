// Модуль реализует основную логику приложения. 
// Модуль экспортирует singleton - объект класса GuiApi, у которого есть единственный публичный метод
// httpProcessor - функция обработки http запросов отправленных на URL /api..., с единственным параметром
//      httpExchange - объект, связанный с обрабатываемым HTTP-запросом (см. httpexchange.js). 

const logger = require('logger');
const os = require('os');

const SDWANCFG_PREFIX_LENGTH = 10;      // Длина (в символах) префикса, добавляемого перед JSON в сообщениях SDWANCFG API 
                                        // (см. http://gitrepo.zelax.local/root/SD-WAN/blob/master/doc/sdwan-api-readme.md)
const SDWANCFG_TIME_TO_RESPONCE_S = 5;      // Время ожидания ответа от SDWANCFG API (сек.)
const SDWANCFG_SOCKET_FILENAME = '/tmp/zelax_sdwan_controller_socket.socket';    // Имя файла, идентифицирующее IPC socket SDWANCFG API
const SDWANCFG_SOCKET_RECONNECT_INTERVAL_MS = 5000;     // Интервал между попытками подключения к сокету SDWANCFG API

class GuiApi {
    constructor() {
        this.#exchangeStates = new Map();   // Коллекция объектов, хранящих состояния текущих обменов. 
                                            // Объекты добавляется в коллекцию в начале обмена, удаляется при завершении 
                                            // (завершение может произойти по таймеру). 

        this.#ipcClient = new (require('ipcclient'))(   // Интерфейсный объект для работы с IPC сокетом SDWANCFG API
            (data) => this.#ipcReadProcessor(data), SDWANCFG_SOCKET_FILENAME, SDWANCFG_SOCKET_RECONNECT_INTERVAL_MS
            );

        this.#ipcReadStack = {      // Промежуточное хранилище данных получаемых из IPC сокета SDWANCFG API
            buffer: "",
            length: 0
        };

        this.#jsonRpc = new (require('jsonrpc'))();     // Интерфейсный объект для работы с запросами JSON RPC v.2.0
    }

    httpProcessor(httpExchange) {
        let state = {};     // Объект, накапливающий всю информацию об исполнении запроса к GUI API
                            // ( как по обмену между клиентом и сервером GUI API, 
                            // так и по соответствующим обменам по SDWANCFG API), а также дополнительные данные
        state.httpExchange = httpExchange;

        // Маршрутизация HTTP-запроса (выбор функции GUI API)
        if (httpExchange.pathParts[1] === 'cpe') {                      // /api/cpe....
            if (httpExchange.pathParts.length === 2) {                      // /api/cpe
                if (httpExchange.method === 'GET') {                            // GET
                    state.guiApiMethod = 'getCpeList';
                    this.#getCpeList(state);
                    return;
                }
                httpExchange.methodNotAllowed('GET');
                return;
            } 
            if (httpExchange.pathParts.length === 3) {
                state.cpe_id = httpExchange.pathParts[2];                   // /api/cpe/<id>
                switch (httpExchange.method) {
                    case 'GET':                                                   //GET
                        state.guiApiMethod = 'getCpe';
                        this.#getCpe(state);
                        return;
                    case 'PATCH':                                                 // PATCH
                        state.guiApiMethod = 'editCpe';
                        this.#editCpe(state);
                        return;
                    case 'DELETE':                                                // DELETE
                        state.guiApiMethod = 'forgetCpe';
                        this.#forgetCpe(state);
                        return;
                    default:
                        httpExchange.methodNotAllowed('GET,PATCH,DELETE');
                        return;
                }
            }
            if (httpExchange.pathParts.length === 4) {
                if (httpExchange.pathParts[2] === 'config' ) {              // /api/cpe/config/<id>
                    state.cpe_id = httpExchange.pathParts[3];
                    switch (httpExchange.method) {
                        case 'GET':                                               //GET
                            state.guiApiMethod = 'getCpeConfig';
                            this.#getCpeConfig(state);
                            return;
                        case 'PATCH':                                             // PATCH
                            state.guiApiMethod = 'editCpeConfig';
                            this.#editCpeConfig(state);
                            return;
                        case 'PUT':                                               // PUT
                            state.guiApiMethod = 'replaceCpeConfig';
                            this.#replaceCpeConfig(state);
                            return;  
                        default:
                            httpExchange.methodNotAllowed('GET,PATCH,PUT');
                            return;
                    }
                }
            }
        }

        if (httpExchange.pathParts[1] === 'log') {                      // /api/log....
            if (httpExchange.pathParts.length === 2) {                      // /api/log     ||
                if (httpExchange.method === 'GET') {                            // GET
                    state.guiApiMethod = 'getLog';
                    this.#getLog(state);
                    return;
                }
                if (httpExchange.method === 'PATCH') {                          // PATCH
                    state.guiApiMethod = 'editLog';
                    this.#editLog(state);
                    return;
                }
                httpExchange.methodNotAllowed('GET,PATCH');
                return;
            } 
        }

        httpExchange.notFound();
    }

// private //

    // ---------- Функции GUI API

    #getCpeList(state) {
        logger.debug('GuiApi.getCpeList - start');
        state.sdwancfgMethod = 'cpe.get';

        let params = {};
        params.update = this.#ToNum(state.httpExchange.params.update);
        params.filter = ['connection', 'profile'];

        this.#sdwancfgSendRequest(state, params);
        logger.debug('GuiApi.getCpeList - success');
    }

    #getCpe(state) {
        logger.debug('GuiApi.getCpe - start');
        state.sdwancfgMethod = 'cpe.get';

        let params = {};
        params.update = this.#ToNum(state.httpExchange.params.update);
        params.filter = ['connection', 'profile'];
        params.cpe_id = state.cpe_id;

        this.#sdwancfgSendRequest(state, params);
        logger.debug('GuiApi.getCpe - success');
    }

    #editCpe(state) {
        logger.debug('GuiApi.editCpe - start');
        state.sdwancfgMethod = 'cpe.edit';
        let params = {};

        try {
            let p = JSON.parse(state.httpExchange.bodyStr);
            if (p.profile !== undefined) {
                params.profile = p.profile;
            }
        } catch {
            logger.debug('GuiApi.editCpe - body parsing ERROR:', state.httpExchange.bodyStr);
            state.httpExchange.badRequest('Request body is not valid JSON.');
            return;
        }
        params.cpe_id = state.cpe_id;

        this.#sdwancfgSendRequest(state, params);
        logger.debug('GuiApi.editCpe - success');
    }

    #forgetCpe(state) {
        logger.debug('GuiApi.forgetCpe - start');
        state.sdwancfgMethod = 'cpe.forget';
        let params = {};
        params.cpe_id = state.cpe_id;

        this.#sdwancfgSendRequest(state, params);
        logger.debug('GuiApi.forgetCpe - success');
    }

    #getCpeConfig(state) {
        logger.debug('GuiApi.getCpeConfig - start');
        state.sdwancfgMethod = 'cpe.get';

        let params = {};
        params.update = this.#ToNum(state.httpExchange.params.update);
        params.filter = ['config'];
        params.cpe_id = state.cpe_id;

        this.#sdwancfgSendRequest(state, params);
        logger.debug('GuiApi.getCpeConfig - success');
    }

    #editCpeConfig(state) {
        logger.debug('GuiApi.editCpeConfig - start');
        state.sdwancfgMethod = 'cpe.edit';
        let params = {};

        try {
            let p = JSON.parse(state.httpExchange.bodyStr);
            if (p.config !== undefined) {
                params.config = p.config;
            }
        } catch {
            logger.debug('GuiApi.editCpeConfig - body parsing ERROR:', state.httpExchange.bodyStr);
            state.httpExchange.badRequest('Request body is not valid JSON.');
            return;
        }
        params.cpe_id = state.cpe_id;

        this.#sdwancfgSendRequest(state, params);
        logger.debug('GuiApi.editCpeConfig - success');
    }

    #replaceCpeConfig(state) {
        logger.debug('GuiApi.replaceCpeConfig - start');
        state.sdwancfgMethod = 'cpe.replace';
        let params = {};
        try {
            let p = JSON.parse(state.httpExchange.bodyStr);
            if (p.config !== undefined) {
                params.config = p.config;
            }
        } catch {
            logger.debug('GuiApi.replaceCpeConfig - body parsing ERROR:', state.httpExchange.bodyStr);
            state.httpExchange.badRequest('Request body is not valid JSON.');
            return;
        }
        params.cpe_id = state.cpe_id;
        this.#sdwancfgSendRequest(state, params);
        logger.debug('GuiApi.replaceCpeConfig - success');
    }

    #getLog(state) {
        logger.debug('GuiApi.getLog - start');
        state.sdwancfgMethod = 'log.get';
        let params = {};
        params.line_from = this.#ToNum(state.httpExchange.params.line_from);
        params.line_to = this.#ToNum(state.httpExchange.params.line_to);

        this.#sdwancfgSendRequest(state, params);
        logger.debug('GuiApi.getLog - success');
    }

    #editLog(state) {
        logger.debug('GuiApi.editLog - start');
        let params = {};

        try {
            let p = JSON.parse(state.httpExchange.bodyStr);
            if (p.lines_count !== undefined) {
                params.lines_count = p.lines_count;
            }
        } catch {
            logger.debug('GuiApi.editLog - body parsing ERROR:', state.httpExchange.bodyStr);
            state.httpExchange.badRequest('Request body is not valid JSON.');
            return;
        }

        // There is only clear (lines_count == 0) implemented now. Non zero lines_count will be used for log truncation.
        // Now SDWANCFG API supports only clear log function without parameters
        if (params.lines_count !== undefined && params.lines_count == 0) {
            state.sdwancfgMethod = 'log.clear';
            this.#sdwancfgSendRequest(state, undefined);    // params <- undefined
            logger.debug('GuiApi.editLog - success');
        } else {
            let str = 'Request body must be valid JSON with mandatory parameter "lines_count": 0 .';
            state.httpExchange.badRequest(str);
            logger.debug('GuiApi.editLog - error. ' + str);
        }
    }

    // ---------- Другие приватные методы

    // Преобразование строки в число с диагностикой. 
    // Параметр:
    //      s - исходная строка;
    // Возвращаемое значение - полученное в результате преобразования число.
    //      При отсутствие входного параметра, либо, если он имеет значение null, 
    //          функция ничего не возвращает и это не считается ошибкой.
    //      Если входной парамер не null, но не может быть преобразован в число, функция также 
    //          ничего не возвращает, но при этом в лог выдается сообщение об ошибке.
     #ToNum(s) {
        if (s != null) {
            let n = Number(s);
            if (n != NaN) {
                return n;
            } else {
                logger.debug('GuiApi.copyToNum - error conver to number:', s);
            }
        }
    }

    // Отправка запроса в сокет SDWANCFG API - вызывается одной из функций SDWANCFG API
    // Параметры:
    //      state - объект, хранящий состояние обмена;
    //      params - параметры запроса к SDWANCFG API, сформированная в одной из функций GUI API.
    #sdwancfgSendRequest(state, params) {
//        logger.debug('GuiApi.sdwancfgSendRequest - start');
        let save_this = this;   // For using instead of this in callbacks

        let sdwancfgRequest = this.#jsonRpc.pack(state.sdwancfgMethod, params);     // подготовка объекта JSON RPC запроса

        // Представление запроса в виде JSON, завершение обмена в случае ошибки.
        let sdwancfgRequestStr;
        try {
            sdwancfgRequestStr = JSON.stringify(sdwancfgRequest, null, 2);
        } catch {
            logger.debug('GuiApi.sdwancfgSendRequest - error' + '  :::  ' + 'Exception in JSON.stringify(sdwancfgRequest)');
            state.httpExchange.internalServerError('GuiApi.sdwancfgSendRequest error.' + 
                                                        '\nException in JSON.stringify(sdwancfgRequest)');
            return;
        }

        sdwancfgRequestStr = addPrefix(sdwancfgRequestStr);     // Добавить префикс с длиной 

        // Отправка запроса
        if (!this.#ipcClient.write(sdwancfgRequestStr)) {
            logger.debug('GuiApi.sdwancfgSendRequest - error' + '  :::  ' + ' in ipcClient.write');
            state.httpExchange.internalServerError('IPC Client write() error.' + 
                                                        '\nData: ' + sdwancfgRequestStr);
            return;
        }

        // Сохранение объекта состояния обмена в коллекции
        state.id = sdwancfgRequest.id;
        state.method = sdwancfgRequest.method;
        state.sdwancfgRequest = sdwancfgRequest;
        state.sdwancfgRequestStr = sdwancfgRequestStr;
        this.#exchangeStates.set(state.id, state);
        
        // Взведение таймаута на удаление объекта состояния обмена из коллекции.
        // При срабатывании таймера обмен завершается с ошибкой.
        state.timeout = setTimeout(
            (id) => {
                logger.debug('GuiApi.Timeout occur' + '  :::  id=' + id);
                let state = save_this.#exchangeStates.get(id);
                if (state === undefined) {
                    logger.debug('GuiApi.Timeout - error' + '  :::  id not found  :::  id=' + id);
                    return;
                }
                if (!save_this.#exchangeStates.delete(id)) {
                    logger.debug('GuiApi.Timeout - error' + '  :::  delete false  :::  id=' + id);
                }
                state.httpExchange.serviceUnavailable(SDWANCFG_TIME_TO_RESPONCE_S);
            }, 
            SDWANCFG_TIME_TO_RESPONCE_S*1000, 
            state.id);
//            logger.debug('GuiApi.sdwancfgSendRequest - success');
    }

    // Callback, который вызывается при получении порции данных из IPC сокета SDWANCFG API
    #ipcReadProcessor(data) {
//         logger.debug('GuiApi.ipcReadProcessor - start');

        if (data === null) {                        // Reset - особый случай, сброс данных накопленных в буфере. 
            logger.debug('GuiApi.ipcReadProcessor - reset');
            this.#ipcReadStack.buffer = '';
            this.#ipcReadStack.length = 0;
            return;
        }

        this.#ipcReadStack.buffer += data;

        // Если в буфере достаточно данных для разбора префикса - разбор, сохранение результата разбора в ipcReadStack.length. 
        // При ошибке разбора - пересинхронизация, после переподключения к сокету будет 
        // вызвана эта же функция (ipcReadProcessor) с параметром null.
        if (this.#ipcReadStack.length === 0 && this.#ipcReadStack.buffer.length >= SDWANCFG_PREFIX_LENGTH) {
            this.#ipcReadStack.length = parseInt(this.#ipcReadStack.buffer.substring(0, SDWANCFG_PREFIX_LENGTH), 10);
            if (isNaN(this.#ipcReadStack.length)) {
                logger.debug('GuiApi.ipcReadProcessor - data prefix parsing ERROR:', this.#ipcReadStack.buffer);
                this.#ipcClient.reconnect();
                return;
            }
        }
        
        // Если в буфере достаточно данных для разбора сообщения, извлечение сообщения из буфера и передача его на обработку. 
        // Если буфер после этого не пуст - рекурсия.
        if (this.#ipcReadStack.buffer.length >= this.#ipcReadStack.length + SDWANCFG_PREFIX_LENGTH) {
            let jsonStr = this.#ipcReadStack.buffer.substring(SDWANCFG_PREFIX_LENGTH, this.#ipcReadStack.length + SDWANCFG_PREFIX_LENGTH);
            this.#processSdwancfgResponce(jsonStr);
            this.#ipcReadStack.buffer = this.#ipcReadStack.buffer.substring(this.#ipcReadStack.length + SDWANCFG_PREFIX_LENGTH);
            this.#ipcReadStack.length = 0;
            if (this.#ipcReadStack.buffer !== '') this.#ipcReadProcessor('');
        }
//        logger.debug('GuiApi.ipcReadProcessor - success');
    }

    // Обработка сообщения, полученнго от SDWANCFG API
    #processSdwancfgResponce(jsonStr) {
        logger.debug('GuiApi.processSdwancfgResponce - start');
        // Преобразование полученного JSON в объект.
        // При ошибке разбора JSON - пересинхронизация обменов с SDWANCFG API
        let res;
        try {
            res = JSON.parse(jsonStr);
        } catch {
            logger.debug('GuiApi.processSdwancfgResponce - responce parsing ERROR:', jsonStr);
            this.#ipcClient.reconnect();
            return;
        }

        let checkResult = this.#jsonRpc.check(res);     // Проверка на соответтсвие JSON RPC v.2.0, определение типа ответа

        switch (checkResult) {
            case 'error':
                // Если сообщение не содержит идентификатора дальнейшая обработка невозможна.
                if (res.id === undefined || res.id === null) {
                    logger.debug('GuiApi.processSdwancfgResponce - ERROR' + 
                               '\nRequest id not defined ' +
                               '\nError code: ' + String(res.error.code) +
                               '\nError message: ' + String(res.error.message) +
                               '\nResponse: ' + jsonStr);
                    return;
                }
                // Отсутствие break или его аналога - не ошибка.
            case 'result':
                // Извлекаем из коллекции объект состояния обмена по id
                let state = this.#exchangeStates.get(res.id);
                if (state === undefined) {
                    logger.debug('GuiApi.processSdwancfgResponce' + '  :::  state by id not found (may be deleted by time)  :::  id=' + String(res.id));
                    return;
                }
                if (!this.#exchangeStates.delete(state.id)) {
                    logger.debug('GuiApi.processSdwancfgResponce - error' + '  :::  delete false  :::  id=' + String(res.id));
                }
                clearTimeout(state.timeout);    // Останавливаем таймер на удаление найденного объекта состояния обмена

                state.sdwancfgResponce = res;
                state.sdwancfgResponceStr = jsonStr;

                // Дальнейшая обработка различна для успешного выполнения JSON RPС запроса и ошибки при его выполнении
                if (checkResult === 'result') {
                    this.#processSdwacfgMethodResult(state);
                } else {
                    this.#processSdwacfgMethodError(state);
                }
                logger.debug('GuiApi.processSdwancfgResponce - success');
                return;
            case 'fatal':
            default:
                // При фатальных ошибках - пересинхронизация обменов с SDWANCFG API
                this.#ipcClient.reconnect();
                logger.debug('GuiApi.processSdwancfgResponce - responce fatal ERROR:', jsonStr);
                return;
            }
    }

    #processSdwacfgMethodResult(state) {
        let data;
        switch (state.guiApiMethod) {
            case 'getCpeList':
            case 'getCpe':
                data = state.sdwancfgResponce.result.data;
                if (data != null) {
                    data.forEach((cpe) => addCpeLinks(cpe, cpe.cpe_id));
                }            
                state.httpExchange.successWithJson(state.sdwancfgResponce.result);
                return;
            case 'editCpe':
                switch (state.sdwancfgMethod) {
                    case 'cpe.edit':
                        this.#getCpe(state);
                        return;
                    case 'cpe.get':
                        data = state.sdwancfgResponce.result.data;
                        if (data != null) {
                            data.forEach((cpe) => addCpeLinks(cpe, cpe.cpe_id));
                        }
                        state.httpExchange.successWithJson(state.sdwancfgResponce.result);
                        return;
                    default:
                        state.httpExchange.internalServerError('Error in backend: function processSdwacfgMethodResult, guiApiMethod ' + 
                                                                state.guiApiMethod + ', sdwancfgMethod ' + state.sdwancfgMethod);
                        return;
                }
            case 'forgetCpe':
                state.httpExchange.noContent();
                return;
            case 'getCpeConfig':
                data = state.sdwancfgResponce.result.data;
                if (data != null) {
                    data.forEach((cpe) => addCpeConfigLinks(cpe, cpe.cpe_id));
                }            
                state.httpExchange.successWithJson(state.sdwancfgResponce.result);
                return;
            case 'editCpeConfig':
                switch (state.sdwancfgMethod) {
                    case 'cpe.edit':
                        this.#getCpeConfig(state);
                        return;
                    case 'cpe.get':
                        data = state.sdwancfgResponce.result.data;
                        if (data != null) {
                            data.forEach((cpe) => addCpeConfigLinks(cpe, cpe.cpe_id));
                        }            
                        state.httpExchange.successWithJson(state.sdwancfgResponce.result);
                        return;
                    default:
                        state.httpExchange.internalServerError('Error in backend: function processSdwacfgMethodResult, guiApiMethod ' + 
                                                                state.guiApiMethod + ', sdwancfgMethod ' + state.sdwancfgMethod);
                        return;
                }
            case 'replaceCpeConfig':
                switch (state.sdwancfgMethod) {
                    case 'cpe.replace':
                        this.#getCpeConfig(state);
                        return;
                    case 'cpe.get':
                        data = state.sdwancfgResponce.result.data;
                        if (data != null) {
                            data.forEach((cpe) => addCpeConfigLinks(cpe, cpe.cpe_id));
                        }            
                        state.httpExchange.successWithJson(state.sdwancfgResponce.result);
                        return;
                    default:
                        state.httpExchange.internalServerError('Error in backend: function processSdwacfgMethodResult, guiApiMethod ' + 
                                                                state.guiApiMethod + ', sdwancfgMethod ' + state.sdwancfgMethod);
                        return;
                }
            case 'getLog':
                state.httpExchange.successWithJson(state.sdwancfgResponce.result);
                return;
            case 'editLog':
                state.httpExchange.noContent();
                return;
            default:
                state.httpExchange.internalServerError('Error in backend: function processSdwacfgMethodResult, guiApiMethod ' + state.guiApiMethod);
                return;
        }
    }

    #processSdwacfgMethodError(state) {
/*
## Таблица обозначения ошибок - http://gitrepo.zelax.local/root/SD-WAN/blob/master/doc/sdwan-api-readme.md



| Code   | Message                       | Data                   | Meaning                                                      |
| ------ | ----------------------------- | ---------------------- | ------------------------------------------------------------ |
| -32700 | Parse error                   |                        | Некорректный формат JSON'a в запросе. Сервер не смог разобрать JSON. |
| -32600 | Invalid Request               |                        | Отсутствует версия jsonrpc или id запроса.        |
| -32601 | Method not found              |                        | Указанный method не известен или поле method отсутствует в запросе.      |
| -32602 | Invalid params                | { "path": <string> }   | Параметр запроса не соответствует схеме. В data возвращается путь к ошибочному элементу параметра. |
| -32004 | Required field missing        | { "fields": [<string>] } | Отсутствуют требуемые поля в параметрах запроса. В data возвращается массив отсутствующих полей. |
| -32000 | Unknown field                 | { "field": }           | В запросе присутствует не описанное в протоколе поле. В data возвращается это поле.
| 10001  | Database error                |                        | Ошибка при обращении к базе данных. |
| 10002  | Failed to connect to controller |                        | Ошибка при попытке подключиться к контроллеру. Необходимо убедиться, что контроллер запущен. |
| 10003  | Internal sdwancfg error       | { "details": <string>}    | Сообщение выдается при внутренних проблемах обработки сообщения в контроллере. |
| 10004  | Invalid update number         |                        | Update number отличается от update number контроллера более чем на 10000. |
| 10005  | Update number changed         |                        | Во время получения конфигурации что-то поменялось. Необходимо повторить запрос. |
| 10006  | Log file access error         |                        | Не получилось открыть,прочитать или изменить файл с логом контроллера. Необходимо отправить запрос log.clear |
| 10007  | Log line number out of range  |                        | Пришедший в запросе номер строки меньше единицы или номер первой строки лога больше реального количества строк в файле. |
| 10008  | Invalid log line range        |                        | Пришедший в запросе номер начальной строки лога больше, чем номер конечной строки лога |
| 10010  | Can't get CPE data because the CPE isn't found | { "cpe_id": <string> } | Выдается при попытке получить данные о несуществующем CPE. |
| 10012  | Can't restore the original CPE configuration during the recovery from a failed request. See log for more details | { "cpe_id": <string> } | Сообщение выдается при возникновении ошибки в процессе восстановления исходной конфигурации CPE после неудачной попытки выполнить запрос. |
| 10013  | Can't restore the CPE synchronization lock flag while processing the configuration replace request. See log for more details | { "cpe_id": } | Сообщение выдается при возникновении ошибки восстановления флага блокировки синхронизации конфигурации с CPE при выполнении запроса замены конфигурации CPE.
| 20001  | Internal error                | { "details": <string> }   | Сообщение выдается при внутренних проблемах обработки сообщения в контроллере. |
| 20009  | Can't change CPE data because the CPE isn't found | { "cpe_id": <string> } | Выдается при попытке изменить настройки отсутствующего в контроллере CPE. |
| 20024  | Can't configure the CPE because new CPE configuration doesn't match the YANG model | { "cpe_id": <string>, "details": <string> } | Сообщение выдается при попытке изменения конфигурации, если итоговая конфигурация не прошла проверку по YANG модели. details содержит детали ошибки, сгенерированные библиотекой libyang. |
| 20025  | Can't configure the CPE because the configuration source is set to "cpe-once" | { "cpe_id": <string> }  | Сообщение выдается при попытке настройки устройства, для которого установлен флаг однократной загрузки конфигурации с устройства. |
| 20026  | Can't remove configuration subtree because it doesn't exist | { "cpe_id": <string>, "config_name": <string> }  | Сообщение выдается при попытке удалить поддерево конфигурации по несуществующему пути. |
| 20027  | Can't forget the CPE because the CPE is registered | { "cpe_id": <string> } | Сообщение выдается, при попытке забыть зарегистрированное CPE. |
| 20028  | Can't configure the CPE because complex element validation failed | { "cpe_id": <string>, "details": <string> } | Сообщение выдается при попытке изменения конфигурации, если итоговая конфигурация содержит некорректные составные ЭК. details содержит детали ошибки. |
| 20029  | Can't cancel registration because the CPE is not registered | { "cpe_id": <string> } | Сообщение выдается, при попытке отменить регистрацию незарегистрированного устройства. |
| 20030  | Can't configure the CPE because specified path is incorrect | { "cpe_id": , "path": , "details": } | Сообщение выдается, при попытке изменения конфигурации, если указанный путь некорректен. details содержит детали ошибки.

*/
        if (state.sdwancfgResponce.error.code == 10004) {
            state.httpExchange.resetContent();
            return;
        }

        if (state.sdwancfgResponce.error.code == 10005) {
            state.httpExchange.serviceUnavailable(0);
            return;
        }

        if (state.sdwancfgResponce.error.code == 10010 || 
            state.sdwancfgResponce.error.code == 20009) {
            state.httpExchange.notFound();
            return;
        }

        if (state.sdwancfgResponce.error.code == 10007 || 
            state.sdwancfgResponce.error.code == 10008 || 
            state.sdwancfgResponce.error.code == 20024 || 
            state.sdwancfgResponce.error.code == 20025 || 
            state.sdwancfgResponce.error.code == 20026 || 
            state.sdwancfgResponce.error.code == 20027 || 
            state.sdwancfgResponce.error.code == 20028 || 
            state.sdwancfgResponce.error.code == 20029 ||
            state.sdwancfgResponce.error.code == 20030 ) {
            state.httpExchange.conflict(String(state.sdwancfgResponce.error.message) + 
                                        '\nAdditional Data: ' + JSON.stringify(state.sdwancfgResponce.error.data, null, 4));
            return;
        }

        let s = '[' + String(state.sdwancfgResponce.error.code) + ']' + String(state.sdwancfgResponce.error.message);
        if (state.sdwancfgResponce.error.data != null) {
            s += '\nAdditional Data: ' + JSON.stringify(state.sdwancfgResponce.error.data, null, 4);
        }
        state.httpExchange.internalServerError(s);
    }

    #exchangeStates
    #ipcClient
    #ipcReadStack
    #jsonRpc
}

module.exports = new GuiApi;

function addPrefix(str) {
    let prefix = str.length.toString().padStart(SDWANCFG_PREFIX_LENGTH, "0");
    return prefix + str;
}

function addCpeLinks(obj, id) {
    obj.links = [];
    obj.links[0] = {};
    obj.links[0].rel = "self";
    obj.links[0].uri = "/api/cpe/" + id;
    obj.links[1] = {};
    obj.links[1].rel = "cpe_config";
    obj.links[1].uri = "/api/cpe/config/" + id;
}

function addCpeConfigLinks(obj, id) {
    obj.links = [];
    obj.links[0] = {};
    obj.links[0].rel = "self";
    obj.links[0].uri = "/api/cpe/config/" + id;
    obj.links[1] = {};
    obj.links[1].rel = "cpe";
    obj.links[1].uri = "/api/cpe/" + id;
}

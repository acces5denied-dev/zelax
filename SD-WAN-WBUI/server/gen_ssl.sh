#!/bin/bash

CERT_DIR="./cert"
CERT_FILE_CA="sd_wan_wbui_ca"
CERT_NAME_CA="SD_WAN_WBUI_CA"

CERT_FILE_CLIENT="localhost"
CERT_NAME_CLIENT="SD_WAN_WBUI_CLIENT"

#если нет корневого сертификата, то это означает что скрипт запускается впервые или он был удален вручную
if [ ! -e ${CERT_DIR}/${CERT_FILE_CA}.crt ]; then
  #на случай если сертификат был удален, мы удаляем все сертификаты, так как они уже не актуальны
  rm -r ${CERT_DIR}/*

  #Создать файлы сертификата и ключа валидностью на 100 лет
  openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 36500 -keyout ${CERT_DIR}/${CERT_FILE_CA}.key \
  -out ${CERT_DIR}/${CERT_FILE_CA}.crt -subj "/C=RU/ST=Moscow/L=Zelenograd/O=Zelax/CN=${CERT_NAME_CA}"

  #создаем пустой файл в котором будут хранится [alt_names] для сертификата приложения
  touch ${CERT_DIR}/alt_names.ext

  echo "The root certificate has been successfully generated"
fi

#если файла нет, то это означает что скрипт запускается впервые
if [ ! -e ${CERT_DIR}/alt_names.ext ]; then
  #Создать файлы сертификата и ключа валидностью на 100 лет
  openssl req -x509 -sha256 -nodes -newkey rsa:2048 -days 36500 -keyout ${CERT_DIR}/${CERT_FILE_CA}.key \
  -out ${CERT_DIR}/${CERT_FILE_CA}.crt -subj "/C=RU/ST=Moscow/L=Zelenograd/O=Zelax/CN=${CERT_NAME_CA}"

  #создаем пустой файл в котором будут хранится [alt_names] для сертификата приложения
  touch ${CERT_DIR}/alt_names.ext
  echo "The root certificate has been successfully generated"
fi

#помещаем в переменную значения из файла
OLD_ALT_NAMES=`cat ${CERT_DIR}/alt_names.ext`

#текущие ip адреса сервера
IPS=`ip -4 -o addr show scope global | awk '{gsub(/\/.*/,"",$4); print $4}'`

#Добавляем новые [alt_names] для сертификата приложения,
# которые будут добавлены в процессе его подписания
alt_names=""
index=1
for ip in $IPS
do
  alt_names="$alt_names IP.$index=$ip"
  index=$(( $index + 1 ))
done

echo "authorityKeyIdentifier=keyid,issuer
      basicConstraints=CA:FALSE
      subjectAltName=@alt_names
      [alt_names]
      $alt_names" \
  > ${CERT_DIR}/alt_names.ext

#получаем значения [alt_names] записанные шагом выше
NEW_ALT_NAMES=`cat ${CERT_DIR}/alt_names.ext`

#если значения не равны, значит поменялся [alt_names] или эта первый запуск скрипта,
# следовательно нужно сгенерировать и подписать новые сертификаты для приложения
if [ "$NEW_ALT_NAMES" != "$OLD_ALT_NAMES" ]; then
  #Создать приватный ключ для приложения
  openssl genrsa -out ${CERT_DIR}/${CERT_FILE_CLIENT}.key 2048

  #Создать запрос на подпись сертификата для приватного ключа .csr
  openssl req -key ${CERT_DIR}/${CERT_FILE_CLIENT}.key \
  -new -out ${CERT_DIR}/${CERT_FILE_CLIENT}.csr -subj "/C=RU/ST=Moscow/L=Zelenograd/O=Zelax/CN=${CERT_NAME_CLIENT}"

  #Подписать сертификат корневым сертификатом sd_wan_wbui_ca.crt
  #и получить в результате новый сертификат для приложения .crt (срок валидности 100 лет)
  openssl x509 -req -CA ${CERT_DIR}/${CERT_FILE_CA}.crt -CAkey ${CERT_DIR}/${CERT_FILE_CA}.key -in ${CERT_DIR}/${CERT_FILE_CLIENT}.csr \
  -out ${CERT_DIR}/${CERT_FILE_CLIENT}.crt -days 36500 -extfile ${CERT_DIR}/alt_names.ext

  #удаляем промежуточный файл .csr для сертификата
  rm ${CERT_DIR}/${CERT_FILE_CLIENT}.csr

  echo "Certificates have been successfully generated"
else
  echo "There is no need to change the certificates"
fi
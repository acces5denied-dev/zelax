## Сборка zelax sdwan gui

#### 1. Клонирование репозитория

```bash
git clone git@gitrepo.zelax.local:root/SD-WAN-WBUI.git <path_to_repo>
cd <path_to_repo>
```


#### 2. Подключение submodule

```bash
git submodule init
git submodule update
```

#### 3. Запустить скрипт make_gui_deb.sh

> По окончанию работы скрипта сборки, файл .deb будет находиться в папке из которой запускался скрипт сборки

    <path_to_repo>/scripts/build/make_gui_deb.sh

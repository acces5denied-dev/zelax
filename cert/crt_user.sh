#!/bin/sh
#
# Создать сертификаты, подписанный нашим rootCA
# =============================================================================
# Перед запуском скрипта нужно:
# 1) сгенерировать корневой сертификат;
# 2) убедиться, что в директории со скриптом лежат файлы корневого сертификата:
#    * rootCA.crt;
#    * rootCA.key;
#    * rootCApub.key.
# =============================================================================

# параметры сертификата контроллера
controllerCrtName="controller"
controllerCNField="CONTROLLER"
controllerSNField=0

# параметры сертификата CPE-агента
cpeCrtName="cpe-agent"
cpeCNFieldBase="zelax"
cpeCount=100

createCertificate() {
  local crtName=$1
  local CNField=$2
  local SNField=$3

  subjStr="/serialNumber=$SNField/CN=$CNField/L=Zelenograd/C=RU"

  # (1) создать приватный ключ
  openssl genrsa -out $crtName.key 2048
  # проверить приватный ключ
  openssl rsa -noout -in $crtName.key -check

  # (2) создать запрос на сертификат
  openssl req -new -key $crtName.key -out $crtName.csr -subj $subjStr

  # (3) из запроса на сертификат создать сертификат, подписанный приватным ключом rootCA.key
  openssl x509 -req -days 10000 -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -in $crtName.csr -out $crtName.crt
}

checkCertificate() {
  local crtName=$1

  # проверить что сертификат подписан CA сертификатом 
  openssl verify -verbose -CAfile rootCA.crt $crtName.crt

  # проверить что публичный ключ один и тот же в приватном ключе, в сертификате и запросе на сертификат
  privateKeyOne=$(openssl rsa -in $crtName.key -pubout -outform pem | sha256sum)
  privatekeyTwo=$(openssl x509 -noout -in $crtName.crt -pubkey -outform pem | sha256sum)
  privateKeyThree=$(openssl req -noout -in $crtName.csr -pubkey -outform pem | sha256sum)

  if [ "$privateKeyOne" != "$privatekeyTwo" -o "$privatekeyTwo" != "$privateKeyThree" ]
  then
    echo "Uncorrected public keys: $crtName"
  fi

  # проверить, что приватный и публичный ключи соответствуют друг другу
  privateKey=$(openssl rsa -noout -modulus -in $crtName.key | sha256sum)
  publicKey=$(openssl x509 -noout -modulus -in $crtName.crt | sha256sum)

  if [ "$privateKey" != "$publicKey" ]
  then
    echo "Uncorrected private and public keys: $crtName"
  fi
}

# создаем сертификат для контроллера
createCertificate $controllerCrtName $controllerCNField $controllerSNField
checkCertificate $controllerCrtName

# создаем сертификаты для CPE-агентов
for it in $(seq 1 $cpeCount); do
  crtName=$cpeCrtName.$it
  CNField=$cpeCNFieldBase$it
  SNField=$it

  createCertificate $crtName $CNField $SNField
  checkCertificate $crtName
done

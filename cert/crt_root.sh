#!/bin/sh
#
# Создать корневые: приватный ключ, сертификат, открытый ключ
# =============================================================================
# Проверка соединений между клиентом и сервером с использованием TLS/SSL:
# 1) Запускаем отладочный ssl-сервер при помощи openssl:
#    openssl s_server -accept 443 -cert rootCA.crt -key rootCA.key -state
# 2) На стороне клиента обращаемся к серверу, например, culr’ом:
#    curl -k https://127.0.0.1:443
# =============================================================================
# Полезные ссылки:
# http://blog.regolit.com/2010/02/16/personal-ca-and-self-signed-certificates
# =============================================================================

# (1) Создать приватный ключ
# Приватный ключ рекомендуется защищать паролем (парольной фразой), иначе злоумышленник, которому удалось украсть у вас приватный ключ, сможет без проблем залогиниться на вашем сервере. 
# ? нужно ли шифровать приватный ключ? использовать rand? пароль? или для тестирования и так сойдет?
  openssl genrsa -out rootCA.key 2048

# Проверить приватный ключ:
  openssl rsa -noout -in rootCA.key -check

# (2) Создать CA сертификат на основе приватного ключа
# ! вынести в конфиг эти параметры для сертификата
# ? где тут самоподписание?
  openssl req -x509 -new -key rootCA.key -days 10000 -out rootCA.crt -subj '/C=RU/ST=MSK/L=Moscow/O=Zelax/OU=SDWAN/CN=zelax.ru/emailAddress=sdndev@zelax.ru'
# Если эта команда с параметром subj генерирует не корректный ключ, чтобы его восстановить - нужно прочитать и записать ключ
  openssl rsa -in rootCA.key -out rootCA.key

# Проверить что приватный ключ соответствует сертификату:
  privateKeyOne=$(openssl pkey -in rootCA.key -pubout -outform pem | sha256sum)
  privateKeyOne=$(openssl x509 -in rootCA.crt -pubkey -noout -outform pem | sha256sum)

  if [ "$privateKeyOne" != "$privateKeyOne" ]
  then
    echo "Uncorrected private key!"
  fi

# (3) Создать публичный ключ на основе приватного 
  openssl rsa -in rootCA.key -out rootCApub.key -pubout
